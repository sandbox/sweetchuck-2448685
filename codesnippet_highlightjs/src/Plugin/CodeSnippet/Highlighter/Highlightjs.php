<?php
/**
 * @file
 * Drupal\codesnippet_highlightjs\Plugin\CodeSnippet\Highlighter\Highlightjs.
 */

namespace Drupal\codesnippet_highlightjs\Plugin\CodeSnippet\Highlighter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Annotation\Translation;
use Drupal\codesnippet\Annotation\CodeSnippetHighlighter;
use Drupal\codesnippet\Plugin\CodeSnippet\HighlighterBase;
use Drupal\filter\FilterProcessResult;

/* @codingStandardsIgnoreStart */
/**
 * Highlight.js executor.
 *
 * @see https://highlightjs.org
 *
 * @CodeSnippetHighlighter(
 *   id = "highlightjs",
 *   title = @Translation("Highlight.js"),
 *   description = @Translation("Javascript syntax highlighter https://highlightjs.org."),
 *   links = {
 *     "codelanguage_map" = "codesnippet_highlightjs.admin.highlighter.highlightjs.codelanguage_map"
 *   }
 * )
 *
 * @package Drupal\codesnippet_highlightjs\Plugin\CodeSnippet\highlighter
 */
class Highlightjs extends HighlighterBase {
  /* @codingStandardsIgnoreEnd */

  /**
   * {@inheritdoc}
   */
  protected $internalLanguageLabels = [
    '1c' => '1c',
    'actionscript' => 'ActionScript',
    'apache' => 'Apache2 configuration',
    'applescript' => 'AppleScript',
    'asciidoc' => 'AsciiDoc',
    'aspectj' => 'AspectJ',
    'autohotkey' => 'AutoHotkey',
    'avrasm' => 'avrasm',
    'axapta' => 'axapta',
    'bash' => 'Bash',
    'brainfuck' => 'Brainfuck',
    'cs' => 'C#',
    'cpp' => 'C++',
    'capnproto' => 'capnproto',
    'clojure' => 'Clojure',
    'clojure-repl' => 'Clojure repl',
    'cmake' => 'CMake',
    'coffeescript' => 'CoffeeScript',
    'css' => 'CSS',
    'd' => 'D',
    'scilab' => 'Dcilab',
    'delphi' => 'Delphi',
    'diff' => 'Diff or patch',
    'django' => 'Django',
    'dos' => 'dos',
    'dust' => 'dust',
    'erb' => 'erb',
    'erlang' => 'Erlang',
    'erlang-repl' => 'Erlang repl',
    'fsharp' => 'F#',
    'fix' => 'fix',
    'gcode' => 'gcode',
    'gherkin' => 'Gherkin',
    'glsl' => 'glsl',
    'go' => 'Go',
    'gradle' => 'gradle',
    'groovy' => 'Groovy',
    'haml' => 'haml',
    'handlebars' => 'handlebars',
    'haskell' => 'Haskell',
    'haxe' => 'haXe',
    'http' => 'http',
    'ini' => 'INI',
    'java' => 'Java',
    'javascript' => 'JavaScript',
    'json' => 'JSON',
    'lasso' => 'Lasso',
    'less' => 'LESS',
    'lisp' => 'Lisp',
    'livecodeserver' => 'livecodeserver',
    'livescript' => 'LiveScript',
    'lua' => 'lua',
    'makefile' => 'Makefile',
    'markdown' => 'Markdown',
    'mathematica' => 'mathematica',
    'matlab' => 'matlab',
    'mel' => 'mel',
    'mercury' => 'mercury',
    'mizar' => 'mizar',
    'monkey' => 'monkey',
    'nginx' => 'Nginx',
    'nimrod' => 'nimrod',
    'nix' => 'nix',
    'nsis' => 'nsis',
    'objectivec' => 'Objective C',
    'ocaml' => 'ocaml',
    'oxygene' => 'oxygene',
    'parser3' => 'parser3',
    'perl' => 'Perl',
    'php' => 'PHP',
    'powershell' => 'powershell',
    'processing' => 'processing',
    'profile' => 'profile',
    'protobuf' => 'protobuf',
    'puppet' => 'puppet',
    'python' => 'Python',
    'q' => 'Q',
    'r' => 'R',
    'rib' => 'rib',
    'elixir' => 'Rlixir',
    'roboconf' => 'RoboConf',
    'rsl' => 'rsl',
    'ruby' => 'Ruby',
    'ruleslanguage' => 'ruleslanguage',
    'rust' => 'rust',
    'dart' => 'Sart',
    'scala' => 'Scala',
    'scheme' => 'scheme',
    'scss' => 'SCSS',
    'smali' => 'smali',
    'smalltalk' => 'Smalltalk',
    'sml' => 'sml',
    'sql' => 'SQL',
    'stata' => 'stata',
    'step21' => 'step21',
    'stylus' => 'Stylus',
    'swift' => 'Swift',
    'tcl' => 'Tcl',
    'tex' => 'TeX',
    'thrift' => 'thrift',
    'twig' => 'Twig',
    'typescript' => 'TypeScript',
    'vala' => 'vala',
    'vbnet' => 'VB.Net',
    'vbscript-html' => 'VB Script',
    'vbscript' => 'VB Script',
    'verilog' => 'Verilog',
    'vhdl' => 'vhdl',
    'vim' => 'vim',
    'x86asm' => 'x86asm',
    'xl' => 'xl',
    'xml' => 'XML',
  ];

  /**
   * {@inheritdoc}
   */
  public function filterProcess(FilterProcessResult $result, $code_body_flat, array $options) {
    return $this->highlight($code_body_flat, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function highlight($code_body, array $options = []) {
    $options += $this->getDefaultHighlightOptions();

    $options['codelanguage_id'] = $this->codeLanguageGlobal2Internal($options['codelanguage_id']);
    $options['output_format'] = $this->getOutputFormat($options['output_format']);

    if (!$options['codelanguage_id'] || !$options['output_format']) {
      return FALSE;
    }

    $plugin_id = $this->getPluginId();
    $plugin_definition = $this->getPluginDefinition();

    $prefix = sprintf('<pre><code class="language-%s">', Html::getClass($options['codelanguage_id']));
    $suffix = '</code></pre>';

    return [
      'library' => [
        "{$plugin_definition['provider']}/$plugin_id",
      ],
      'body' => $prefix . htmlentities($code_body) . $suffix,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function availableInternalCodeLanguages($environment) {
    switch ($environment) {
      case NULL:
      case 'client_js':
        // @todo Cache.
        // @todo Use the service container.
        /** @var \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery */
        $library_discovery = \Drupal::service('library.discovery');
        $library = $library_discovery->getLibraryByName('codesnippet_highlightjs', 'highlightjs');

        if (!$library) {
          return [];
        }

        $file_name = NULL;
        foreach ($library['js'] as $options) {
          if ($options['type'] === 'file' && preg_match('@/highlight\.pack\.js$@', $options['data'])) {
            $file_name = $options['data'];

            break;
          }
        }

        if (!$file_name) {
          return [];
        }

        $pattern = '/;hljs\.registerLanguage\("(?P<name>[^"]+)",function\(/';
        $matches = [];
        if (preg_match_all($pattern, file_get_contents($file_name), $matches)) {
          $code_languages = array_combine($matches['name'], $matches['name']);
          $code_languages = array_intersect_key($this->internalLanguageLabels, $code_languages) + $code_languages;
          asort($code_languages, SORT_NATURAL | SORT_FLAG_CASE);

          return $code_languages;
        }

        break;
    }

    return [];
  }

}
