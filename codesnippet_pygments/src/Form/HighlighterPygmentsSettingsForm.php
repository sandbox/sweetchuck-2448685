<?php
/**
 * @file
 * Home of \Drupal\codesnippet_pygments\Form\SettingsForm.
 */

namespace Drupal\codesnippet_pygments\Form;

use Drupal\codesnippet\System;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\codesnippet_pygments\Form
 */
class HighlighterPygmentsSettingsForm extends ConfigFormBase {

  use System;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'codesnippet_pygments_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('codesnippet_pygments.highlighter.pygments.settings');

    $form['pygmentize_path'] = [
      '#type' => 'textfield',
      '#title' => t('Pygmentize path'),
      '#required' => TRUE,
      '#default_value' => $config->get('pygmentize_path'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $pygmentize_path = $form_state->getValue('pygmentize_path');
    $pygmentize_version = $this->pygmentizeVersion($pygmentize_path);
    if ($pygmentize_version) {
      $form_state->set(['codesnippet_pygments', 'pygmentize_version'], $pygmentize_version);
    }
    else {
      $form_state->setErrorByName('pygmentize_path', $this->t('Pygmentize version cannot be determined'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('codesnippet_pygments.highlighter.pygments.settings');
    $config
      ->set('pygmentize_path', $form_state->getValue('pygmentize_path'))
      ->save();

    $pygmentize_version = $form_state->get(['codesnippet_pygments', 'pygmentize_version']);
    drupal_set_message($this->t('Pygmentize version is @version', ['@version' => $pygmentize_version]));

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['codesnippet_pygments.highlighter.pygments.settings'];
  }

  /**
   * Detect the version of the pygmentize executable.
   *
   * @param string $pygmentize_path
   *   File path.
   *
   * @return string|bool
   *   Version string or FALSE.
   */
  protected function pygmentizeVersion($pygmentize_path) {
    $cmd = sprintf('%s -V', escapeshellcmd($pygmentize_path));

    return $this->detectVersion($cmd, '/^Pygments version (?P<version>\d.+?),/');
  }

  /**
   * Detect the version number in the output.
   *
   * @param string $cmd
   *   The command that will be executed.
   * @param string $pattern
   *   Regular expression with a named group "version".
   * @param int $line_index
   *   Number of the line in the output which contains the version number.
   *
   * @return string|bool
   *   Version string or FALSE.
   */
  protected function detectVersion($cmd, $pattern, $line_index = 0) {
    $matches = [];
    $result = $this->exec($cmd);

    return ($result['error_code'] === 0
      && isset($result['output'][$line_index])
      && preg_match($pattern, $result['output'][$line_index], $matches)
    ) ? $matches['version'] : FALSE;
  }

}
