<?php
/**
 * @file
 * Home of \Drupal\codesnippet_pygments\Plugin\CodeSnippet\highlighter\Pygments.
 */

namespace Drupal\codesnippet_pygments\Plugin\CodeSnippet\Highlighter;

use Drupal\codesnippet\System;
use Drupal\Core\Annotation\Translation;
use Drupal\codesnippet\Annotation\CodeSnippetHighlighter;
use Drupal\codesnippet\Plugin\CodeSnippet\HighlighterBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\filter\FilterProcessResult;

/* @codingStandardsIgnoreStart */
/**
 * Pygmentize executor.
 *
 * @see http://pygments.org/docs/cmdline
 *
 * @CodeSnippetHighlighter(
 *   id = "pygments",
 *   title = @Translation("Pygments"),
 *   description = @Translation("CLI tool of Pygments."),
 *   links = {
 *     "settings"         = "codesnippet_pygments.admin.highlighter.pygments.settings",
 *     "codelanguage_map" = "codesnippet_pygments.admin.highlighter.pygments.codelanguage_map"
 *   }
 * )
 *
 * @package Drupal\codesnippet_pygments\Plugin\CodeSnippet\highlighter
 */
class Pygments extends HighlighterBase {
  /* @codingStandardsIgnoreEnd */

  use System;

  /**
   * {@inheritdoc}
   */
  protected $internalLanguageLabels = array(
    'abap' => 'ABAP',
    'as' => 'ActionScript',
    'as3' => 'ActionScript 3',
    'ada' => 'Ada',
    'antlr' => 'ANTLR',
    'antlr-as' => 'ANTLR With ActionScript Target',
    'antlr-csharp' => 'ANTLR With C# Target',
    'antlr-cpp' => 'ANTLR With C++ Target',
    'antlr-java' => 'ANTLR With Java Target',
    'antlr-objc' => 'ANTLR With ObjectiveC Target',
    'antlr-perl' => 'ANTLR With Perl Target',
    'antlr-python' => 'ANTLR With Python Target',
    'antlr-ruby' => 'ANTLR With Ruby Target',
    'apacheconf' => 'Apache2 Configuration',
    'applescript' => 'AppleScript',
    'aspectj' => 'AspectJ',
    'aspx-cs' => 'ASPX C#',
    'aspx-vb' => 'ASPX VB',
    'asy' => 'Asymptote',
    'ahk' => 'AutoHotkey',
    'autoit' => 'AutoIt',
    'awk' => 'AWK',
    'basemake' => 'Base Makefile',
    'bash' => 'Bash',
    'console' => 'Bash Session',
    'bat' => 'Batch',
    'bbcode' => 'BBCode',
    'befunge' => 'Befunge',
    'blitzmax' => 'BlitzMax',
    'boo' => 'Boo',
    'brainfuck' => 'Brainfuck',
    'bro' => 'Bro',
    'bugs' => 'BUGS',
    'c' => 'C',
    'csharp' => 'C#',
    'cpp' => 'C++',
    'c-objdump' => 'C Object dump',
    'ca65' => 'ca65',
    'cbmbas' => 'CBM BASIC V2',
    'ceylon' => 'Ceylon',
    'cfengine3' => 'CFEngine3',
    'cfs' => 'cfstatement',
    'cheetah' => 'Cheetah',
    'clojure' => 'Clojure',
    'cmake' => 'CMake',
    'cobol' => 'COBOL',
    'cobolfree' => 'COBOLFree',
    'coffee-script' => 'CoffeeScript',
    'cfm' => 'Coldfusion HTML',
    'common-lisp' => 'Common Lisp',
    'coq' => 'Coq',
    'cpp-objdump' => 'C++ Object dump',
    'croc' => 'Croc',
    'css' => 'CSS',
    'css+django' => 'CSS Django/Jinja',
    'css+genshitext' => 'CSS Genshi Text',
    'css+lasso' => 'CSS Lasso',
    'css+mako' => 'CSS Mako',
    'css+myghty' => 'CSS Myghty',
    'css+php' => 'CSS PHP',
    'css+erb' => 'CSS Ruby',
    'css+smarty' => 'CSS Smarty',
    'cuda' => 'CUDA',
    'cython' => 'Cython',
    'd' => 'D',
    'd-objdump' => 'D Object dump',
    'dpatch' => 'Darcs Patch',
    'dart' => 'Dart',
    'control' => 'Debian Control file',
    'sourceslist' => 'Debian Sourcelist',
    'delphi' => 'Delphi',
    'dg' => 'dg',
    'diff' => 'Diff',
    'django' => 'Django/Jinja',
    'dtd' => 'DTD',
    'duel' => 'Duel',
    'dylan' => 'Dylan',
    'dylan-lid' => 'DylanLID',
    'dylan-console' => 'Dylan session',
    'ec' => 'eC',
    'ecl' => 'ECL',
    'elixir' => 'Elixir',
    'iex' => 'Elixir iex session',
    'ragel-em' => 'Embedded Ragel',
    'erb' => 'ERB',
    'erlang' => 'Erlang',
    'erl' => 'Erlang erl session',
    'evoque' => 'Evoque',
    'factor' => 'Factor',
    'fancy' => 'Fancy',
    'fan' => 'Fantom',
    'felix' => 'Felix',
    'fortran' => 'Fortran',
    'Clipper' => 'FoxPro',
    'fsharp' => 'F#',
    'gas' => 'GAS',
    'genshi' => 'Genshi',
    'genshitext' => 'Genshi Text',
    'pot' => 'Gettext Catalog',
    'Cucumber' => 'Gherkin',
    'glsl' => 'GLSL',
    'gnuplot' => 'Gnuplot',
    'go' => 'Go',
    'gooddata-cl' => 'GoodData-CL',
    'gosu' => 'Gosu',
    'gst' => 'Gosu Template',
    'groff' => 'Groff',
    'groovy' => 'Groovy',
    'haml' => 'Haml',
    'haskell' => 'Haskell',
    'hx' => 'haXe',
    'html' => 'HTML',
    'html+cheetah' => 'HTML Cheetah',
    'html+django' => 'HTML Django/Jinja',
    'html+evoque' => 'HTML Evoque',
    'html+genshi' => 'HTML Genshi',
    'html+lasso' => 'HTML Lasso',
    'html+mako' => 'HTML Mako',
    'html+myghty' => 'HTML Myghty',
    'html+php' => 'HTML PHP',
    'html+smarty' => 'HTML Smarty',
    'html+velocity' => 'HTML Velocity',
    'http' => 'HTTP',
    'haxeml' => 'Hxml',
    'hybris' => 'Hybris',
    'idl' => 'IDL',
    'ini' => 'INI',
    'io' => 'Io',
    'ioke' => 'Ioke',
    'irc' => 'IRC logs',
    'jade' => 'Jade',
    'jags' => 'JAGS',
    'java' => 'Java',
    'js' => 'JavaScript',
    'js+cheetah' => 'JavaScript Cheetah',
    'js+django' => 'JavaScript Django/Jinja',
    'js+genshitext' => 'JavaScript Genshi Text',
    'js+lasso' => 'JavaScript Lasso',
    'js+mako' => 'JavaScript Mako',
    'js+myghty' => 'JavaScript Myghty',
    'js+php' => 'JavaScript PHP',
    'js+erb' => 'JavaScript Ruby',
    'js+smarty' => 'JavaScript Smarty',
    'jsp' => 'JSP',
    'json' => 'JSON',
    'julia' => 'Julia',
    'jlcon' => 'Julia console',
    'kconfig' => 'Kconfig',
    'koka' => 'Koka',
    'kotlin' => 'Kotlin',
    'lasso' => 'Lasso',
    'lighty' => 'Lighttpd configuration file',
    'lhs' => 'Haskell (Literate)',
    'live-script' => 'LiveScript',
    'llvm' => 'LLVM',
    'logos' => 'Logos',
    'logtalk' => 'Logtalk',
    'lua' => 'Lua',
    'make' => 'Makefile',
    'mako' => 'Mako',
    'maql' => 'MAQL',
    'mason' => 'Mason',
    'matlab' => 'Matlab',
    'matlabsession' => 'Matlab session',
    'minid' => 'MiniD',
    'modelica' => 'Modelica',
    'modula2' => 'Modula-2',
    'trac-wiki' => 'MoinMoin/Trac Wiki markup',
    'monkey' => 'Monkey',
    'moocode' => 'MOOCode',
    'moon' => 'MoonScript',
    'mscgen' => 'Mscgen',
    'mupad' => 'MuPAD',
    'mxml' => 'MXML',
    'myghty' => 'Myghty',
    'mysql' => 'MySQL',
    'nasm' => 'NASM',
    'nemerle' => 'Nemerle',
    'newlisp' => 'NewLisp',
    'newspeak' => 'Newspeak',
    'nginx' => 'Nginx configuration',
    'nimrod' => 'Nimrod',
    'nsis' => 'NSIS',
    'numpy' => 'NumPy',
    'objdump' => 'Object dump',
    'objective-c' => 'Objective C',
    'objective-c++' => 'Objective C++',
    'objective-j' => 'Objective J',
    'ocaml' => 'OCaml',
    'octave' => 'Octave',
    'ooc' => 'Ooc',
    'opa' => 'Opa',
    'openedge' => 'OpenEdge ABL',
    'perl' => 'Perl',
    'php' => 'PHP',
    'plpgsql' => 'PL/pgSQL',
    'psql' => 'PostgreSQL console',
    'postgresql' => 'PostgreSQL SQL dialect',
    'postscript' => 'PostScript',
    'pov' => 'POVRay',
    'powershell' => 'PowerShell',
    'prolog' => 'Prolog',
    'properties' => 'Properties',
    'protobuf' => 'Protocol Buffer',
    'puppet' => 'Puppet',
    'pypylog' => 'PyPy Log',
    'python' => 'Python',
    'python3' => 'Python 3',
    'py3tb' => 'Python 3.0 Traceback',
    'pycon' => 'Python console session',
    'pytb' => 'Python Traceback',
    'qml' => 'QML',
    'racket' => 'Racket',
    'ragel' => 'Ragel',
    'ragel-c' => 'Ragel in C Host',
    'ragel-cpp' => 'Ragel in C++ Host',
    'ragel-d' => 'Ragel in D Host',
    'ragel-java' => 'Ragel in Java Host',
    'ragel-objc' => 'Ragel in Objective C Host',
    'ragel-ruby' => 'Ragel in Ruby Host',
    'raw' => 'Raw token data',
    'rconsole' => 'RConsole',
    'rd' => 'Rd',
    'rebol' => 'REBOL',
    'redcode' => 'Redcode',
    'registry' => 'reg',
    'rst' => 'reStructuredText',
    'rhtml' => 'RHTML',
    'RobotFramework' => 'RobotFramework',
    'spec' => 'RPMSpec',
    'rb' => 'Ruby',
    'rbcon' => 'Ruby irb session',
    'rust' => 'Rust',
    'splus' => 'S',
    'sass' => 'SASS',
    'scala' => 'Scala',
    'ssp' => 'Scalate Server Page',
    'scaml' => 'Scaml',
    'scheme' => 'Scheme',
    'scilab' => 'Scilab',
    'scss' => 'SCSS',
    'shell-session' => 'Shell Session',
    'smali' => 'Smali',
    'smalltalk' => 'Smalltalk',
    'smarty' => 'Smarty',
    'snobol' => 'Snobol',
    'sp' => 'SourcePawn',
    'sql' => 'SQL',
    'sqlite3' => 'Sqlite3 con',
    'squidconf' => 'SquidConf',
    'stan' => 'Stan',
    'sml' => 'Standard ML',
    'systemverilog' => 'SystemVeriLog',
    'tcl' => 'Tcl',
    'tcsh' => 'Tcsh',
    'tea' => 'Tea',
    'tex' => 'TeX',
    'text' => 'Text only',
    'treetop' => 'Treetop',
    'ts' => 'TypeScript',
    'urbiscript' => 'UrbiScript',
    'vala' => 'Vala',
    'vb.net' => 'VB.Net',
    'velocity' => 'Velocity',
    'verilog' => 'Verilog',
    'vgl' => 'VGL',
    'vhdl' => 'vhdl',
    'vim' => 'Vim',
    'xml' => 'XML',
    'xml+cheetah' => 'XML Cheetah',
    'xml+django' => 'XML Django/Jinja',
    'xml+evoque' => 'XML Evoque',
    'xml+lasso' => 'XML Lasso',
    'xml+mako' => 'XML Mako',
    'xml+myghty' => 'XML Myghty',
    'xml+php' => 'XML PHP',
    'xml+erb' => 'XML Ruby',
    'xml+smarty' => 'XML Smarty',
    'xml+velocity' => 'XML Velocity',
    'xquery' => 'XQuery',
    'xslt' => 'XSLT',
    'xtend' => 'Xtend',
    'yaml' => 'Yaml',
  );

  /**
   * Filesystem path to the "rougify" executable.
   *
   * @var string
   */
  protected $rougifyPath = '';

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $config_factory);

    $config = $this->getGlobalConfig();
    $this->setRougifyPath = $config->get('rougify_path');
  }

  /**
   * Get path of the 'rougify' executable.
   *
   * @return string
   *   File path.
   */
  public function getRougifyPath() {
    return $this->rougifyPath;
  }

  /**
   * Set path of the 'rougify' executable.
   *
   * @param string $value
   *   New value.
   *
   * @return $this
   *   The called object.
   */
  public function setRougifyPath($value) {
    $this->rougifyPath = $value;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function filterProcess(FilterProcessResult $result, $code_body_flat, array $options) {
    return $this->highlight($code_body_flat, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function highlight($code_body, array $options = []) {
    $options += $this->getDefaultHighlightOptions();

    $options['codelanguage_id'] = $this->codeLanguageGlobal2Internal($options['codelanguage_id']);
    $options['output_format'] = $this->getOutputFormat($options['output_format']);

    $pygmentize_path = $this->getGlobalConfig()->get('pygmentize_path');

    if (!$options['codelanguage_id']
      || !$options['output_format']
      || !$pygmentize_path
    ) {
      return FALSE;
    }

    $cmd_pattern = 'echo %s | %s -f %s -l %s';
    $cmd_args = [
      escapeshellarg($code_body),
      escapeshellcmd($pygmentize_path),
      escapeshellarg($options['output_format']),
      escapeshellarg($options['codelanguage_id']),
    ];

    $formatter_options = [];
    if ($options['show_line_numbers']) {
      $formatter_options['linenos'] = 'linenos=1';
    }

    if ($formatter_options) {
      $cmd_pattern .= ' -O %s';
      $cmd_args[] = escapeshellarg(implode(',', $formatter_options));
    }

    $result = $this->exec(vsprintf($cmd_pattern, $cmd_args));

    return $result['error_code'] ? FALSE : ['body' => implode("\n", $result['output'])];
  }

  /**
   * {@inheritdoc}
   */
  public function availableInternalCodeLanguages($environment) {
    switch ($environment) {
      case NULL:
      case 'backend':
        // @todo Cache.
        $config = $this->getGlobalConfig();
        $cmd_pattern = '%s -L lexer';
        $cmd_args = [
          escapeshellcmd($config->get('pygmentize_path')),
        ];

        $result = $this->exec(vsprintf($cmd_pattern, $cmd_args));

        if ($result['error_code']) {
          return [];
        }

        $num_of_lines = count($result['output']);
        // The first four line is a header, like this',
        // Pygments version 1.6, (c) 2006-2013 by Georg Brandl.
        //
        // Lexers:
        // ~~~~~~~
        $code_languages = [];
        for ($i = 4; $i < $num_of_lines; $i += 2) {
          list($id) = explode(', ', substr($result['output'][$i], 2, -1));
          $label = $id;
          $n = $i + 1;
          if (isset($result['output'][$n]) && preg_match('/^\s+(?P<label>[^\(]+)/', $result['output'][$n], $matches)) {
            $label = trim($matches['label']);
          }

          $code_languages[$id] = $label;
        }
        $code_languages = array_intersect_key($this->internalLanguageLabels, $code_languages) + $code_languages;
        asort($code_languages, SORT_NATURAL | SORT_FLAG_CASE);

        return $code_languages;

    }

    return [];
  }

}
