<?php

/**
 * @file
 * CodeSnippet API documentation.
 */

/**
 * Alter the CodeSnippet highlighter definitions.
 *
 * @param array $info
 *   Key is the highlighter id.
 *
 * @see \Drupal\codesnippet\Annotation\CodeSnippetHighlighter
 */
function hook_codesnippet_highlighter_info_alter(array &$info) {
  // @todo Example.
  if (isset($info['rouge'])) {
    $info['rouge']['label'] = 'Rougify';
  }
}
