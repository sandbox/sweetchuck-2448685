<?php
/**
 * @file
 * Home of \Drupal\codesnippet_rouge\Plugin\CodeSnippet\Highlighter\Rouge.
 */

namespace Drupal\codesnippet_rouge\Plugin\CodeSnippet\Highlighter;

use Drupal\codesnippet\System;
use Drupal\Core\Annotation\Translation;
use Drupal\codesnippet\Annotation\CodeSnippetHighlighter;
use Drupal\codesnippet\Plugin\CodeSnippet\HighlighterBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\filter\FilterProcessResult;

/* @codingStandardsIgnoreStart */
/**
 * Rougify executor.
 *
 * @see https://github.com/jneen/rouge
 *
 * @CodeSnippetHighlighter(
 *   id = "rouge",
 *   title = @Translation("Rouge"),
 *   description = @Translation("CLI tool of Rouge."),
 *   links = {
 *     "settings"         = "codesnippet_rouge.admin.highlighter.rouge.settings",
 *     "codelanguage_map" = "codesnippet_rouge.admin.highlighter.rouge.codelanguage_map"
 *   }
 * )
 *
 * @package Drupal\codesnippet_rouge\Plugin\CodeSnippet\highlighter
 */
class Rouge extends HighlighterBase {
  /* @codingStandardsIgnoreEnd */

  use System;

  /**
   * {@inheritdoc}
   */
  protected $internalLanguageLabels = [
    'apache' => 'Apache configuration',
    'applescript' => 'AppleScript',
    'c' => 'C programming language',
    'clojure' => 'Clojure',
    'coffeescript' => 'Coffeescript',
    'literate_coffeescript' => 'CoffeeScript (literate)',
    'common_lisp' => 'Common Lisp',
    'conf' => 'A generic lexer for configuration files ',
    'cpp' => 'C++',
    'csharp' => 'C#',
    'css' => 'CSS',
    'dart' => 'Dart',
    'diff' => 'Diffs or patches',
    'elixir' => 'Elixir',
    'erb' => 'Embedded ruby template files',
    'erlang' => 'Erlang',
    'factor' => 'Factor',
    'gherkin' => 'Gherkin',
    'go' => 'Go',
    'groovy' => 'Groovy',
    'haml' => 'Haml templating system for Ruby ',
    'handlebars' => 'Handlebars and Mustache',
    'haskell' => 'Haskell',
    'literate_haskell' => 'Haskell (literate)',
    'html' => 'HTML',
    'http' => 'Http requests and responses',
    'ini' => 'INI',
    'io' => 'IO',
    'java' => 'Java',
    'javascript' => 'JavaScript',
    'json' => 'JSON',
    'llvm' => 'LLVM',
    'lua' => 'Lua',
    'make' => 'Makefile',
    'markdown' => 'Markdown',
    'matlab' => 'Matlab ',
    'moonscript' => 'Moonscript ',
    'nginx' => 'Nginx configuration',
    'nim' => 'Nim',
    'objective_c' => 'Objective C',
    'ocaml' => 'Objective CAML ',
    'perl' => 'Perl',
    'php' => 'PHP',
    'plaintext' => 'Plain text',
    'prolog' => 'Prolog',
    'properties' => '.properties config files for Java',
    'puppet' => 'Puppet',
    'python' => 'Python',
    'qml' => 'QML',
    'r' => 'R',
    'racket' => 'Racket',
    'ruby' => 'Ruby',
    'rust' => 'Rust',
    'sass' => 'SASS',
    'scala' => 'Scala',
    'scheme' => 'Scheme variant of Lisp',
    'scss' => 'SCSS',
    'sed' => 'sed - Stream editor',
    'shell' => 'Shell languages (sh, bash, ...)',
    'slim' => 'Slim',
    'smalltalk' => 'Smalltalk',
    'sml' => 'Standard ML',
    'sql' => 'SQL',
    'swift' => 'Swift',
    'tcl' => 'TCL',
    'tex' => 'TeX typesetting',
    'toml' => 'TOML configuration format',
    'vb' => 'Visual Basic',
    'viml' => 'VimL, Vim editor scripting',
    'xml' => 'XML',
    'yaml' => 'Yaml',
  ];

  /**
   * Filesystem path to the "ruby" executable.
   *
   * @var string
   */
  protected $rubyPath = '';

  /**
   * Filesystem path to the "rougify" executable.
   *
   * @var string
   */
  protected $rougifyPath = '';

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $config_factory);

    $config = $this->getGlobalConfig();
    $this->setRubyPath($config->get('ruby_path'));
    $this->setRougifyPath($config->get('rougify_path'));
  }

  /**
   * Get path to the "ruby" executable.
   *
   * @return string
   *   Filesystem path to the "ruby" executable.
   */
  public function getRubyPath() {
    return $this->rubyPath;
  }

  /**
   * Set path of the "ruby" executable.
   *
   * @param string $value
   *   The new path.
   *
   * @return $this
   */
  public function setRubyPath($value) {
    $this->rubyPath = $value;

    return $this;
  }

  /**
   * Get path to the "rougify" executable.
   *
   * @return string
   *   Filesystem path to the "rougify" executable.
   */
  public function getRougifyPath() {
    return $this->rougifyPath;
  }

  /**
   * Set path of the "rougify" executable.
   *
   * @param string $value
   *   The new path.
   *
   * @return $this
   */
  public function setRougifyPath($value) {
    $this->rougifyPath = $value;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function filterProcess(FilterProcessResult $result, $code_body_flat, array $options) {
    return $this->highlight($code_body_flat, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function highlight($code_body, array $options = []) {
    $options += $this->getDefaultHighlightOptions();

    $options['codelanguage_id'] = $this->codeLanguageGlobal2Internal($options['codelanguage_id']);
    $options['output_format'] = $this->getOutputFormat($options['output_format']);

    $ruby_path = $this->getRubyPath();
    $rougify_path = $this->getRougifyPath();

    if (!$options['codelanguage_id']
      || !$options['output_format']
      || !$rougify_path
    ) {
      return FALSE;
    }

    $cmd_pattern = 'echo %s | %s %s --lexer %s --formatter %s';
    $cmd_args = [
      escapeshellarg($code_body),
      escapeshellcmd($ruby_path),
      escapeshellcmd($rougify_path),
      escapeshellarg($options['codelanguage_id']),
      escapeshellarg($options['output_format']),
    ];

    $formatter_options = [];
    if ($options['show_line_numbers']) {
      $formatter_options['line_numbers'] = 'line_numbers=1';
    }

    if ($formatter_options) {
      $cmd_pattern .= ' --formatter-opts %s';
      $cmd_args[] = escapeshellarg(implode('&', $formatter_options));
    }

    $cmd = vsprintf($cmd_pattern, $cmd_args) . ' -';
    $result = $this->exec($cmd);

    return $result['error_code'] ? FALSE : ['body' => implode("\n", $result['output'])];
  }

  /**
   * {@inheritdoc}
   *
   * @todo Cache.
   */
  public function availableInternalCodeLanguages($environment) {
    switch ($environment) {
      case NULL:
      case 'backend':
        $ruby_path = $this->getRubyPath();
        $rougify_path = $this->getRougifyPath();

        if (!$ruby_path || !$rougify_path) {
          return [];
        }

        $cmd_pattern = '%s %s list';
        $cmd_args = [
          escapeshellcmd($ruby_path),
          escapeshellarg($rougify_path),
        ];

        $cmd = vsprintf($cmd_pattern, $cmd_args);
        $result = $this->exec($cmd);

        if ($result['error_code']) {
          return [];
        }

        $pattern = '/^(?P<id>.+?): (?P<label>[^\(\[]+)(\((?P<url>[^)]+)\)){0,1}( *\[aliases: (?P<aliases>.+?)\]){0,1}/im';
        $num_of_lines = count($result['output']);
        $code_languages = [];
        for ($i = 1; $i < $num_of_lines; $i += 2) {
          $matches = [];
          if (preg_match($pattern, $result['output'][$i], $matches)) {
            $code_languages[$matches['id']] = $matches['label'];
          }
        }
        $code_languages = array_intersect_key($this->internalLanguageLabels, $code_languages) + $code_languages;
        asort($code_languages, SORT_NATURAL | SORT_FLAG_CASE);

        return $code_languages;

    }

    return [];
  }

}
