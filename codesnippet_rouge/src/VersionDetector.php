<?php
/**
 * @file
 * Home of \Drupal\codesnippet_rouge\VersionDetector.
 */

namespace Drupal\codesnippet_rouge;

use Drupal\codesnippet\System;

/**
 * Class VersionDetector.
 *
 * @package Drupal\codesnippet_rouge
 */
class VersionDetector {

  use System;

  /**
   * Detect the version of the ruby executable.
   *
   * @param string $ruby_path
   *   File path.
   *
   * @return string|bool
   *   Version string or FALSE.
   */
  public function rubyVersion($ruby_path) {
    if (!$ruby_path) {
      return FALSE;
    }

    $cmd = sprintf('%s --version', escapeshellcmd($ruby_path));

    return $this->detectVersion($cmd, '/^ruby (?P<version>\d.+?)( |$)/');
  }

  /**
   * Detect the version of the rougify executable.
   *
   * @param string $ruby_path
   *   File path.
   * @param string $rougify_path
   *   File path.
   *
   * @return string|bool
   *   Version string or FALSE.
   */
  public function rougifyVersion($ruby_path, $rougify_path) {
    if (!$ruby_path || !$rougify_path) {
      return FALSE;
    }

    $cmd = sprintf('%s %s --version', escapeshellcmd($ruby_path), escapeshellarg($rougify_path));

    return $this->detectVersion($cmd, '/(?P<version>\d.+?)( |$)/');
  }

  /**
   * Detect the version number in the output.
   *
   * @param string $command
   *   The command that will be executed.
   * @param string $pattern
   *   Regular expression with a named group "version".
   * @param int $line_index
   *   Number of the line in the output which contains the version number.
   *
   * @return string|bool
   *   Version string or FALSE.
   */
  protected function detectVersion($command, $pattern, $line_index = 0) {
    $matches = [];
    $result = $this->exec($command);

    return ($result['error_code'] === 0
      && isset($result['output'][$line_index])
      && preg_match($pattern, $result['output'][$line_index], $matches)
    ) ? $matches['version'] : FALSE;
  }

}
