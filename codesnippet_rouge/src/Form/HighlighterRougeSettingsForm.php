<?php
/**
 * @file
 * Home of \Drupal\codesnippet_rouge\Form\SettingsForm.
 */

namespace Drupal\codesnippet_rouge\Form;

use Drupal\codesnippet_rouge\VersionDetector;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\codesnippet_rouge\Form
 */
class HighlighterRougeSettingsForm extends ConfigFormBase {

  /**
   * Name of the simple configuration entity.
   *
   * @var string
   */
  protected $configName = 'codesnippet_rouge.highlighter.rouge.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'codesnippet_rouge_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config($this->configName);

    $form['ruby_path'] = [
      '#type' => 'textfield',
      '#title' => t('Ruby path'),
      '#required' => TRUE,
      '#default_value' => $config->get('ruby_path'),
    ];

    $form['rougify_path'] = [
      '#type' => 'textfield',
      '#title' => t('Rougify path'),
      '#required' => TRUE,
      '#default_value' => $config->get('rougify_path'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $vd = new VersionDetector();
    $ruby_path = $form_state->getValue('ruby_path');
    $ruby_version = $vd->rubyVersion($ruby_path);
    if ($ruby_version) {
      $form_state->set(['codesnippet_rouge', 'ruby_version'], $ruby_version);

      $rougify_path = $form_state->getValue('rougify_path');
      $rougify_version = $vd->rougifyVersion($ruby_path, $rougify_path);
      if ($rougify_version) {
        $form_state->set(['codesnippet_rouge', 'rougify_version'], $rougify_version);
      }
      else {
        $form_state->setErrorByName(
          'rougify_path',
          $this->t('@application version cannot be determined', ['@application' => 'Rougify'])
        );
      }
    }
    else {
      $form_state->setErrorByName(
        'ruby_path',
        $this->t('@application version cannot be determined', ['@application' => 'Ruby'])
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config($this->configName)
      ->set('ruby_path', $form_state->getValue('ruby_path'))
      ->set('rougify_path', $form_state->getValue('rougify_path'))
      ->save();

    $ruby_version = $form_state->get(['codesnippet_rouge', 'ruby_version']);
    $rougify_version = $form_state->get(['codesnippet_rouge', 'rougify_version']);
    drupal_set_message($this->t('Ruby version is @version', ['@version' => $ruby_version]));
    drupal_set_message($this->t('Rougify version is @version', ['@version' => $rougify_version]));

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [$this->configName];
  }

}
