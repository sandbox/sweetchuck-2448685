<?php
/**
 * @file
 * Home of \Drupal\codesnippet\Entity\ColorSchemeEditForm.
 */

namespace Drupal\codesnippet\Entity;

/**
 * Class ColorSchemeEditForm.
 */
class ColorSchemeEditForm extends ColorSchemeAddForm {

  /**
   * Page title.
   *
   * @param ColorSchemeInterface $codesnippet_colorscheme
   *   ColorScheme configuration entity being edited.
   *
   * @return string
   *   Page title.
   */
  public function pageTitle(ColorSchemeInterface $codesnippet_colorscheme) {
    return $this->t(
      'Edit %label color scheme',
      [
        '%label' => $codesnippet_colorscheme->label(),
      ]
    );
  }

}
