<?php
/**
 * @file
 * Home of \Drupal\codesnippet\Entity\ColorSchemeMapForm.
 */

namespace Drupal\codesnippet\Entity;

use Drupal\codesnippet\Plugin\CodeSnippet\HighlighterManagerInterface;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ColorSchemeMapForm.
 *
 * @package Drupal\codesnippet\From
 */
class ColorSchemeMapForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.codesnippet.highlighter'),
      $container->get('library.discovery'),
      $container->get('entity.manager')->getStorage('codesnippet_colorscheme')
    );
  }

  /**
   * Highlighter manager instance.
   *
   * @var HighlighterManagerInterface
   */
  protected $highlighterManager = NULL;

  /**
   * Library discovery instance.
   *
   * @var LibraryDiscoveryInterface
   */
  protected $libraryDiscovery = NULL;

  /**
   * ColorScheme storage instance.
   *
   * @var ColorSchemeStorageInterface
   */
  protected $colorSchemeStorage = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    HighlighterManagerInterface $highlighter_manager,
    LibraryDiscoveryInterface $library_discovery,
    ColorSchemeStorageInterface $colorscheme_storage
  ) {
    parent::__construct($config_factory);
    $this->highlighterManager = $highlighter_manager;
    $this->libraryDiscovery = $library_discovery;
    $this->colorSchemeStorage = $colorscheme_storage;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'codesnippet_colorscheme_map_form';
  }

  /**
   * Router title callback.
   *
   * @param ColorSchemeInterface $codesnippet_colorscheme
   *   ColorScheme id.
   *
   * @return string
   *   Page title.
   */
  public function getFormLabel(ColorSchemeInterface $codesnippet_colorscheme) {
    return $this->t(
      'Assign libraries to @name color scheme',
      ['@name' => $codesnippet_colorscheme->label()]
    );
  }

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
   *
   * @SuppressWarnings(PHPMD.CyclomaticComplexity)
   *
   * @SuppressWarnings(PHPMD.NPathComplexity)
   */
  public function buildForm(array $form, FormStateInterface $form_state, ColorSchemeInterface $codesnippet_colorscheme = NULL) {
    $highlighter_definitions = $this->highlighterManager->getDefinitions();
    $library_options = $this->libraryOptions();

    if (!$codesnippet_colorscheme || !$highlighter_definitions) {
      return [
        '#markup' => $this->t('Do something because there is no available Code Snippet highlighter plugin.'),
      ];
    }

    /** @var \Drupal\codelanguage\Entity\CodeLanguage[] $codelanguages */
    $codelanguages = entity_load_multiple('codelanguage', NULL);
    $preview_config = $this->config('codesnippet.preview');
    $preview_codelanguage_id = $preview_config->get('codelanguage');
    $preview_codelanguage = (isset($codelanguages[$preview_codelanguage_id]) ?
      $codelanguages[$preview_codelanguage_id] : reset($codelanguages)
    );
    $preview_codelanguage_id = $preview_codelanguage->id();

    $form['preview'] = [
      '#type' => 'details',
      '#title' => $this->t('Preview settings'),
      '#open' => FALSE,
      '#tree' => TRUE,
      'codelanguage' => [
        '#type' => 'select',
        '#required' => FALSE,
        '#title' => $this->t('Code Language to use for preview'),
        '#default_value' => $preview_codelanguage->id(),
        '#empty_option' => $this->t('- None -'),
        '#options' => [],
      ],
      'show_line_numbers' => [
        '#type' => 'checkbox',
        '#title' => t('Show line numbers'),
        '#default_value' => $preview_config->get('show_line_numbers'),
      ],
      'update' => [
        '#type' => 'submit',
        '#value' => $this->t('Update'),
        '#limit_validation_errors' => [
          ['preview', 'codelanguage'],
          ['preview', 'show_line_numbers'],
        ],
        '#submit' => ['::submitFormPreviewUpdate'],
      ],
    ];

    foreach ($codelanguages as $codelanguage) {
      $form['preview']['codelanguage']['#options'][$codelanguage->id()] = $codelanguage->label();
    }

    $form['colorscheme_map'] = [
      '#tree' => TRUE,
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Syntax highlighters'),
    ];

    $libraries = [];
    $code_samples = ((array) $preview_codelanguage->get('samples'));
    $code_sample = (isset($code_samples['syntax_highlight']['code']) ?
      $code_samples['syntax_highlight']['code'] : ''
    );
    /** @var \Drupal\codesnippet\Plugin\CodeSnippet\HighlighterInterface[] $highlighters */
    foreach ($highlighter_definitions as $highlighter_id => $highlighter_definition) {
      $map = $this->config($this->getConfigName($highlighter_definition));
      $form[$highlighter_id] = [
        '#tree' => TRUE,
        '#type' => 'details',
        '#group' => 'colorscheme_map',
        '#parents' => ['colorscheme_map', $highlighter_id],
        '#open' => TRUE,
        '#title' => $highlighter_definition['title'],
        '#attributes' => ['class' => ['highlighter-wrapper']],
        'library' => [
          '#type' => 'select',
          '#parents' => ['colorscheme_map', $highlighter_id, 'library'],
          '#title' => $this->t('Library'),
          '#options' => isset($library_options[$highlighter_id]) ? $library_options[$highlighter_id] : [],
          '#empty_option' => t('- None -'),
          '#default_value' => $map->get($codesnippet_colorscheme->id()),
          '#attributes' => ['class' => ['library-selector']],
        ],
      ];

      if ($this->highlighterManager->isEnabled($highlighter_id)) {
        if (isset($library_options[$highlighter_id])) {
          foreach ($library_options[$highlighter_id] as $library_ids) {
            $libraries = array_merge($libraries, array_keys($library_ids));
          }
        }

        $highlight_result = $this->highlighterManager->highlight(
          $code_sample,
          [
            'colorscheme' => $codesnippet_colorscheme,
            'highlighter_id' => $highlighter_id,
            'codelanguage_id' => $preview_codelanguage_id,
            'show_line_numbers' => $preview_config->get('show_line_numbers'),
          ]
        );

        $form[$highlighter_id]['preview'] = [
          '#type' => 'markup',
          '#markup' => $highlight_result ? $highlight_result['body'] : '<div>' . $this->t('No preview') . '</div>',
        ];

        if (!empty($highlight_result['library'])) {
          $form[$highlighter_id]['preview']['#attached']['library'] = $highlight_result['library'];
        }
      }
      else {
        $form[$highlighter_id]['preview'] = [
          '#type' => 'markup',
          '#markup' => '<div>' . $this->t('This highlighter is not enabled') . '</div>',
        ];
      }
    }

    if ($libraries) {
      $form['colorscheme_map']['#attached']['library'][] = 'codesnippet/colorscheme.map_form';
      $form['colorscheme_map']['#attached']['library'] = array_unique(array_merge(
        $form['colorscheme_map']['#attached']['library'],
        $libraries
      ));
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Submit button handler.
   *
   * @param array $form
   *   Form API form render array.
   * @param FormStateInterface $form_state
   *   Form API state.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function submitFormPreviewUpdate(array &$form, FormStateInterface $form_state) {
    $this->config('codesnippet.preview')
      ->set('codelanguage', $form_state->getValue(['preview', 'codelanguage']))
      ->set('show_line_numbers', (bool) $form_state->getValue(['preview', 'show_line_numbers']))
      ->save();

    $form_state->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();
    $colorscheme_id = $build_info['args'][0]->id();
    $highlighter_definitions = $this->highlighterManager->getDefinitions();
    foreach ($highlighter_definitions as $highlighter_id => $definition) {
      $new_settings = $form_state->getValue(['colorscheme_map', $highlighter_id]);
      $old_config = $this->config($this->getConfigName($definition));

      if (isset($new_settings['library'])
        && $new_settings['library'] != $old_config->get($colorscheme_id)
      ) {
        $old_config
          ->set($colorscheme_id, $new_settings['library'])
          ->save();
      }
    }
  }

  /**
   * Build the config name.
   *
   * @todo This overlaps the HighlighterInterface::getColorSchemeMap().
   *
   * @param array $definition
   *   Plugin definition.
   *
   * @return string
   *   Name of a simple configuration.
   */
  protected function getConfigName(array $definition) {
    return "{$definition['provider']}.highlighter.{$definition['id']}.colorscheme_map";
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $config_names = ['codesnippet.preview'];
    foreach ($this->highlighterManager->getDefinitions() as $definition) {
      $config_names[] = $this->getConfigName($definition);
    }

    return $config_names;
  }

  /**
   * Collect the suitable libraries.
   *
   * @return array
   *   Select options.
   */
  protected function libraryOptions() {
    $options = [];

    foreach (\Drupal::moduleHandler()->getModuleList() as $extension) {
      $module_name = $extension->getName();
      foreach ($this->libraryDiscovery->getLibrariesByExtension($module_name) as $library_id => $library) {
        if (!isset($library['codesnippet']['highlighters'])) {
          continue;
        }

        if (!empty($library['codesnippet']['label'])) {
          $library_label = $library['codesnippet']['label'];
        }
        elseif (!empty($library['label'])) {
          $library_label = $library['label'];
        }
        else {
          $library_label = $library_id;
        }

        foreach (array_keys($library['codesnippet']['highlighters'], TRUE) as $highlighter_id) {
          $options[$highlighter_id][$module_name]["$module_name/$library_id"] = $library_label;
        }
      }
    }

    return $options;
  }

}
