<?php

/**
 * @file
 * Contains \Drupal\codesnippet\Controller\FilterController.
 */

namespace Drupal\codesnippet\Entity;

/**
 * Controller routines for filter routes.
 */
class ColorSchemeController {

  /**
   * Gets the label of a Code Language.
   *
   * @param ColorSchemeInterface $codesnippet_scheme
   *   The Code Snippet Scheme object.
   *
   * @return string
   *   The label of the Code Snippet Scheme.
   */
  public function getLabel(ColorSchemeInterface $codesnippet_scheme) {
    return $codesnippet_scheme->label();
  }

}
