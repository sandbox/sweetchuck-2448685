<?php
/**
 * @file
 * Home of \Drupal\codesnippet\Entity\ColorScheme.
 */

namespace Drupal\codesnippet\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;

/**
 * Class ColorScheme.
 *
 * @ConfigEntityType(
 *   id = "codesnippet_colorscheme",
 *   label = @Translation("Code Snippet Color Scheme"),
 *   config_prefix = "colorscheme",
 *   admin_permission = "administer codesnippet",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "weight" = "weight",
 *   },
 *   handlers = {
 *     "access" = "Drupal\codesnippet\Entity\ColorSchemeAccessControlHandler",
 *     "list_builder" = "Drupal\codesnippet\Entity\ColorSchemeListBuilder",
 *     "storage" = "Drupal\codesnippet\Entity\ColorSchemeStorage",
 *     "form" = {
 *       "add" = "Drupal\codesnippet\Entity\ColorSchemeAddForm",
 *       "edit" = "Drupal\codesnippet\Entity\ColorSchemeEditForm",
 *       "delete" = "Drupal\codesnippet\Entity\ColorSchemeDeleteForm",
 *       "map" = "Drupal\codesnippet\Entity\ColorSchemeMapForm"
 *     }
 *   },
 *   links = {
 *     "add-form" = "entity.codesnippet_colorscheme.add_form",
 *     "edit-form" = "entity.codesnippet_colorscheme.edit_form",
 *     "delete-form" = "entity.codesnippet_colorscheme.delete_form",
 *     "map-form" = "entity.codesnippet_colorscheme.map_form"
 *   }
 * )
 *
 * @package Drupal\codesnippet\Entity
 */
class ColorScheme extends ConfigEntityBase implements ColorSchemeInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    $label = parent::label();

    return $label ?: $this->id();
  }

}
