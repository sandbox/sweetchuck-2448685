<?php
/**
 * @file
 * Home of \Drupal\codesnippet\Entity\ColorSchemeStorage.
 */

namespace Drupal\codesnippet\Entity;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Class ColorSchemeStorage.
 *
 * @package Drupal\codesnippet\Entity
 */
class ColorSchemeStorage extends ConfigEntityStorage implements ColorSchemeStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function getFirst($highlighter_id) {
    $map = \Drupal::config("codesnippet.colorscheme_map.$highlighter_id");
    if (!$map) {
      return NULL;
    }

    $colorscheme_ids = $this->getQuery()->sort('weight', 'ASC')->execute();
    /** @var \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery */
    $library_discovery = \Drupal::service('library.discovery');

    foreach ($colorscheme_ids as $colorscheme_id) {
      $library_id = $map->get($colorscheme_id);
      if (!$library_id) {
        continue;
      }

      list($extension, $library_name) = explode('/', $library_id);
      $library = $library_discovery->getLibraryByName($extension, $library_name);
      if ($library && !empty($library['codesnippet']['highlighters'][$highlighter_id])) {
        return $this->load($colorscheme_id);
      }
    }

    return NULL;
  }

}
