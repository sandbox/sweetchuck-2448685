<?php
/**
 * @file
 * Home of \Drupal\codesnippet\Entity\ColorSchemeListBuilder.
 */

namespace Drupal\codesnippet\Entity;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ColorSchemeListBuilder.
 */
class ColorSchemeListBuilder extends DraggableListBuilder {

  /**
   * The entities being listed.
   *
   * Just override the type hint.
   *
   * @var ColorSchemeInterface[]
   */
  protected $entities = [];

  /**
   * HTML class to add to the weight input element.
   *
   * @var string
   */
  protected $weightClass = '';

  /**
   * {@inheritdoc}
   */
  protected $limit = 0;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage) {
    parent::__construct($entity_type, $storage);

    // Check if the entity type supports weighting.
    if ($this->entityType->hasKey('weight')) {
      $this->weightClass = Html::getClass($this->weightKey);
    }

    $this->entitiesKey = $entity_type->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'codesnippet_colorscheme_overview_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];

    $keys = $this->entityType->getKeys();

    if (!empty($keys['status'])) {
      $header[$keys['status']] = [
        'data' => $this->t('Status'),
      ];
    }

    if (!empty($keys['label'])) {
      $header[$keys['label']] = [
        'data' => $this->t('Name'),
      ];
    }

    $header['description'] = [
      'data' => $this->t('Description'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   *
   * @param EntityInterface $entity
   *   A CodeSnippetColorScheme object to add to the table.
   */
  public function buildRow(EntityInterface $entity) {
    /** @var ConfigEntityInterface $entity */
    $row = [];

    $keys = $this->entityType->getKeys();

    if (!empty($keys['status'])) {
      $row[$keys['status']]['data'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    }

    if (!empty($keys['label'])) {
      $row[$keys['label']] = [
        '#markup' => SafeMarkup::checkPlain($entity->label()),
      ];
    }

    $row['description'] = [
      '#markup' => SafeMarkup::checkPlain($entity->get('description')),
    ];

    $row += parent::buildRow($entity);

    if (!empty($keys['status'])) {
      $row['#attributes']['class'][] = $entity->get('status') ? 'enabled' : 'disabled';
    }

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form[$this->entitiesKey] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => $this->t('There is no @label yet.', ['@label' => $this->entityType->getLabel()]),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $this->weightClass,
        ],
      ],
    ];

    $this->entities = $this->load();
    $delta = 10;
    // Change the delta of the weight field if have more than 20 entities.
    if (!empty($this->weightKey)) {
      $count = count($this->entities);
      if ($count > 20) {
        $delta = ceil($count / 2);
      }
    }

    foreach ($this->entities as $entity) {
      $row = $this->buildRow($entity);

      if (isset($row[$this->weightKey])) {
        $row[$this->weightKey]['#delta'] = $delta;
      }

      $form[$this->entitiesKey][$entity->id()] = $row;
    }

    if ($this->limit) {
      $form['pager'] = [
        '#theme' => 'pager',
      ];
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save order'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // No validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue($this->entitiesKey) as $id => $value) {
      if (isset($this->entities[$id]) && $this->entities[$id]->get($this->weightKey) != $value[$this->weightKey]) {
        $this->entities[$id]->set($this->weightKey, $value[$this->weightKey]);
        $this->entities[$id]->save();
      }
    }
  }

  /**
   * Loads entity IDs without pager.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    if ($this->limit) {
      return parent::getEntityIds();
    }

    return $this
      ->getStorage()
      ->getQuery()
      ->execute();
  }

  /**
   * Gets this list's default operations.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity the operations are for.
   *
   * @return array
   *   The array structure is identical to the return value of
   *   self::getOperations().
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if ($entity->access('update') && $entity->hasLinkTemplate('map-form')) {
      $operations['map'] = [
        'title' => $this->t('Map'),
        'weight' => 10,
        'url' => $entity->urlInfo('map-form'),
      ];
    }

    return $operations;
  }

}
