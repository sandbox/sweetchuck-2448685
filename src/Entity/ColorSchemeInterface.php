<?php
/**
 * @file
 * Home of \Drupal\codesnippet\Entity\ColorSchemeInterface.
 */

namespace Drupal\codesnippet\Entity;

use \Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface ColorSchemeInterface.
 */
interface ColorSchemeInterface extends ConfigEntityInterface {

}
