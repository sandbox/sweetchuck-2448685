<?php
/**
 * @file
 * Home of \Drupal\codesnippet\Entity\ColorSchemeAddForm.
 */

namespace Drupal\codesnippet\Entity;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ColorSchemeAddForm.
 */
class ColorSchemeAddForm extends EntityForm {

  /**
   * The entity being used by this form.
   *
   * @var ColorSchemeInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $keys = $this->entity->getEntityType()->getKeys();

    $form[$keys['label']] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
      '#weight' => -30,
    ];

    $form[$keys['id']] = [
      '#type' => 'machine_name',
      '#required' => TRUE,
      '#default_value' => $this->entity->id(),
      '#maxlength' => 255,
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => [$keys['label']],
      ],
      '#disabled' => !$this->entity->isNew(),
      '#weight' => -20,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#default_value' => $this->entity->get('description'),
    ];

    return $form;
  }

  /**
   * Determines if the Code Snippet Scheme already exists.
   *
   * @param string $id
   *   The Code Snippet Scheme ID.
   *
   * @return bool
   *   TRUE if the Code Snippet Scheme exists, FALSE otherwise.
   */
  public function exists($id) {
    return (bool) $this
      ->entityManager
      ->getStorage($this->entity->getEntityTypeId())
      ->load($id);
  }

}
