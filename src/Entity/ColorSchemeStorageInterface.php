<?php
/**
 * @file
 * Home of \Drupal\codesnippet\Entity\ColorSchemeStorageInterface.
 */

namespace Drupal\codesnippet\Entity;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

/**
 * Interface ColorSchemeStorageInterface.
 *
 * @package Drupal\codesnippet\Entity
 */
interface ColorSchemeStorageInterface extends ConfigEntityStorageInterface {

  /**
   * Get the first color scheme which fits to the given highlighter plugin.
   *
   * @param string $highlighter_id
   *   Highlighter plugin identifier.
   *
   * @return ColorSchemeInterface|null
   *   ColorScheme config entity.
   */
  public function getFirst($highlighter_id);

}
