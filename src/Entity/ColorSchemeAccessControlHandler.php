<?php
/**
 * @file
 * Home of \...\Entity\ColorSchemeAccessControlHandler.
 */

namespace Drupal\codesnippet\Entity;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Class ColorSchemeAccessControlHandler.
 */
class ColorSchemeAccessControlHandler extends EntityAccessControlHandler {

}
