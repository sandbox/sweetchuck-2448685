<?php
/**
 * @file
 * Home of \Drupal\codesnippet\Entity\ColorSchemeDeleteForm.
 */

namespace Drupal\codesnippet\Entity;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class ColorSchemeEditForm.
 *
 * @package Drupal\codesnippet\ConfigEntity
 */
class ColorSchemeDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t(
      'Are you sure you want to delete the %label Color Scheme?',
      [
        '%label' => $this->entity->label(),
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('codesnippet.admin.colorscheme_overview');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    $form_state->setRedirectUrl($this->getCancelUrl());
    drupal_set_message($this->t('Deleted'));
  }

}
