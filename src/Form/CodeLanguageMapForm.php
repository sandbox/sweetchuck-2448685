<?php
/**
 * @file
 * Home of \Drupal\codesnippet\Form\LanguageMapForm.
 */

namespace Drupal\codesnippet\Form;

use Drupal\codesnippet\Plugin\CodeSnippet\HighlighterManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LanguageMapForm.
 *
 * @package Drupal\codesnippet\From
 */
class CodeLanguageMapForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.codesnippet.highlighter')
    );
  }

  /**
   * Highlighter manager instance.
   *
   * @var HighlighterManagerInterface
   */
  protected $highlighterManager = NULL;

  /**
   * Plugin definitions.
   *
   * @var array
   */
  protected $highlighterDefinitions = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, HighlighterManagerInterface $highlighter_manager) {
    parent::__construct($config_factory);
    $this->highlighterManager = $highlighter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'codesnippet_codelanguage_map_form';
  }

  /**
   * Page title.
   *
   * @param string $highlighter_id
   *   Code Snippet highlighter plugin identifier.
   *
   * @return string
   *   Page title.
   */
  public function pageTitle($highlighter_id = '') {
    if (!$highlighter_id) {
      return 'Error';
    }

    $highlighter_definition = $this->highlighterManager->getDefinition($highlighter_id);
    if (!$highlighter_definition) {
      return 'Error';
    }

    return $this->t(
      'Code Language associations of %label',
      [
        '%label' => $highlighter_definition['title'],
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $highlighter_id = '') {
    if ($highlighter_id) {
      $this->highlighterDefinitions = [
        $highlighter_id => $this->highlighterManager->getDefinition($highlighter_id),
      ];
    }
    else {
      $this->highlighterDefinitions = $this->highlighterManager->getEnabledDefinitions();
    }

    if (!$this->highlighterDefinitions) {
      return [
        '#markup' => $this->t('Do something because there is no available Code Snippet highlighter plugin.'),
      ];
    }

    /** @var \Drupal\codelanguage\Entity\CodeLanguage[] $code_languages */
    $code_languages = entity_load_multiple('codelanguage', NULL);

    $form['codelanguage_map'] = [
      '#type' => 'table',
      '#tree' => TRUE,
      '#header' => [
        'codelanguage' => [
          'data' => $this->t('Code Language'),
        ],
      ],
      '#empty' => t('There is no @label yet.', ['@label' => 'Code Snippet highlighter']),
    ];

    /** @var \Drupal\codesnippet\Plugin\CodeSnippet\HighlighterInterface[] $highlighters */
    $highlighters = [];
    $internal_languages = [];
    foreach ($this->highlighterDefinitions as $highlighter_id => $highlighter_definition) {
      $highlighters[$highlighter_id] = $this->highlighterManager->getInstance($highlighter_definition);
      $internal_languages[$highlighter_id] = $highlighters[$highlighter_id]->availableInternalCodeLanguages(NULL);

      $form['codelanguage_map']['#header'][$highlighter_id] = [
        'data' => (string) $highlighter_definition['title'],
      ];
    }

    foreach ($code_languages as $global_lang => $code_language) {
      if (!$code_language->status()) {
        continue;
      }

      $form['codelanguage_map'][$global_lang] = [
        '#tree' => TRUE,
        '#weight' => $code_language->get('weight'),
        'codelanguage' => [
          '#type' => 'markup',
          '#markup' => $code_language->label(),
        ],
      ];

      foreach (array_keys($highlighters) as $highlighter_id) {
        $form['codelanguage_map'][$global_lang][$highlighter_id] = [
          '#type' => 'select',
          '#options' => $internal_languages[$highlighter_id],
          '#empty_option' => $this->t('- None -'),
          '#default_value' => $this->config($this->getConfigName($highlighter_id))->get($global_lang),
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $maps = [];
    foreach ($form_state->getValue('codelanguage_map') as $global_lang => $highlighters) {
      foreach ($highlighters as $highlighter_id => $internal_lang) {
        if (!isset($maps[$highlighter_id])) {
          $maps[$highlighter_id] = $this->config($this->getConfigName($highlighter_id))->get();
        }

        $maps[$highlighter_id][$global_lang] = $internal_lang;
      }
    }

    foreach ($maps as $highlighter_id => $map) {
      $this->config($this->getConfigName($highlighter_id))
        ->setData($map)
        ->save();
    }
  }

  /**
   * Name of the simple configuration which store the color scheme mapping.
   *
   * @param string $highlighter_id
   *   Syntax highlighter backend identifier.
   *
   * @return string
   *   Name of the simple configuration.
   */
  protected function getConfigName($highlighter_id) {
    return $this->highlighterDefinitions[$highlighter_id]['provider'] . ".highlighter.$highlighter_id.colorscheme_map";
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $config_names = [];
    foreach (array_keys($this->highlighterDefinitions) as $id) {
      $config_names[] = $this->getConfigName($id);
    }

    return $config_names;
  }

}
