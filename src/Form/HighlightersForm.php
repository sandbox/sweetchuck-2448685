<?php
/**
 * @file
 * Home of \Drupal\codesnippet\Controller\HighlighterListController.
 */

namespace Drupal\codesnippet\Form;

use Drupal\codesnippet\Plugin\CodeSnippet\HighlighterManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class HighlighterListController.
 *
 * @package Drupal\codesnippet\Controller
 */
class HighlightersForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.codesnippet.highlighter')
    );
  }

  /**
   * Current highlighter manager service.
   *
   * @var HighlighterManagerInterface
   */
  protected $highlighterManager = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, HighlighterManagerInterface $highlighter_manager) {
    parent::__construct($config_factory);
    $this->highlighterManager = $highlighter_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'codesnippet_highlighters_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $definitions = $this->highlighterManager->getDefinitions();

    if (!$definitions) {
      return [
        '#markup' => $this->t('Do something because there is no available Code Snippet highlighter plugin.'),
      ];
    }

    $weight_class = 'weight';
    $delta = ceil(count($definitions) / 2);

    $form['highlighter'] = [
      '#type' => 'table',
      '#tree' => TRUE,
      '#header' => [
        'enabled' => [
          'data' => $this->t('Enabled'),
        ],
        'description' => [
          'data' => $this->t('Description'),
        ],
        'weight' => [
          'data' => $this->t('Weight'),
        ],
        'operations' => [
          'data' => $this->t('Operations'),
        ],
      ],
      '#empty' => t('There is no @label yet.', ['@label' => 'CodeSnippet Highlighter']
      ),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $weight_class,
        ],
      ],
    ];

    foreach ($definitions as $id => $definition) {
      $config = $this->config("codesnippet.highlighter.$id");

      $form['highlighter'][$id] = [
        '#tree' => TRUE,
        '#weight' => $config->get('weight'),
        '#attributes' => [
          'class' => ['draggable'],
        ],
        'enabled' => [
          '#type' => 'checkbox',
          '#title' => (string) $definition['title'],
          '#default_value' => $config->get('enabled'),
        ],
        'description' => [
          '#type' => 'markup',
          '#markup' => (string) $definition['description'],
        ],
        'weight' => [
          '#type' => 'weight',
          '#delta' => $delta,
          '#title' => t('Weight'),
          '#default_value' => $config->get('weight'),
          '#attributes' => ['class' => [$weight_class]],
        ],
        'operations' => [
          '#type' => 'operations',
          '#links' => $this->getOperations($definition),
        ],
      ];
    }

    uasort($form['highlighter'], ['\Drupal\Component\Utility\SortArray', 'sortByWeightProperty']);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $changed_highlighters = [];
    foreach ($form_state->getValue('highlighter', []) as $id => $values) {
      $config = $this->config("codesnippet.highlighter.$id");
      foreach (['enabled', 'weight'] as $key) {
        if ($values[$key] != $config->get($key)) {
          $changed_highlighters[$id] = TRUE;
          $config->set($key, $values[$key]);
        }
      }

      if (!empty($changed_highlighters[$id])) {
        // @todo Clear the affected filter cache.
        $config->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $config_names = [];
    foreach (array_keys($this->highlighterManager->getDefinitions()) as $id) {
      $config_names[] = "codesnippet.highlighter.$id";
    }

    return $config_names;
  }

  /**
   * Get the operations of a highlighter plugin.
   *
   * @todo Move the $links out of this method.
   *
   * @param array $definition
   *   Highlighter definition.
   *
   * @return array
   *   Links.
   */
  protected function getOperations(array $definition) {
    $operations = [];

    $url_params = ['highlighter_id' => $definition['id']];

    $links = [
      'settings' => [
        'title' => $this->t('Settings'),
        'weight' => 0,
      ],
      'codelanguage_map' => [
        'title' => $this->t('Languages'),
        'weight' => 1,
      ],
    ];

    foreach ($links as $link_id => $link) {
      if (!isset($definition['links'][$link_id])) {
        continue;
      }

      $operations[$link_id] = $link + [
        'url' => Url::fromRoute($definition['links'][$link_id], $url_params),
      ];
    }

    uasort($operations, '\Drupal\Component\Utility\SortArray::sortByWeightElement');

    return $operations;
  }

}
