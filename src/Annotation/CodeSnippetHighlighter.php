<?php

/**
 * @file
 * Contains \Drupal\codesnippet\Annotation\CodeSnippetHighlighter.
 */

namespace Drupal\codesnippet\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an CodeSnippet Highlighter annotation object.
 *
 * Plugin Namespace: Plugin\codesnippet\highlighter.
 *
 * @see \Drupal\codesnippet\Plugin\CodeSnippet\Highlighter\Pygments
 *
 * @Annotation
 */
class CodeSnippetHighlighter extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the module providing the type.
   *
   * @var string
   */
  public $provider;

  /**
   * The human-readable name of the Highlighter.
   *
   * This is used as an administrative summary of what the Highlighter does.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * Additional administrative information about the Highlighter's behavior.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation (optional)
   */
  public $description = '';

  /**
   * Operation links.
   *
   * Key is the name, the value is a handler class.
   *
   * @var array
   */
  public $links = [];

}
