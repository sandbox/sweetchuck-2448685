<?php
/**
 * @file
 * Home of \Drupal\codesnippet\System.
 */

namespace Drupal\codesnippet;

/**
 * Class System.
 *
 * @package Drupal\codesnippet
 */
trait System {

  /**
   * Run a shell command with exec().
   *
   * @param string $command
   *   The command string to run.
   *
   * @return array
   *   Keys:
   *   - output: array
   *   - error_code: int
   */
  protected function exec($command) {
    $return = ['output' => [], 'error_code' => 0];
    exec($command, $return['output'], $return['error_code']);

    return $return;
  }

}
