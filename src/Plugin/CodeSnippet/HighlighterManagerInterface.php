<?php
/**
 * @file
 * Home of \Drupal\codesnippet\Plugin\CodeSnippet\HighlighterManagerInterface.
 */

namespace Drupal\codesnippet\Plugin\CodeSnippet;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface HighlighterManagerInterface.
 *
 * @package Drupal\codesnippet\Plugin\CodeSnippet
 */
interface HighlighterManagerInterface extends PluginManagerInterface {

  /**
   * Check the highlighter plugin is enabled or not.
   *
   * @param string|array $plugin_id
   *   Identifier of a plugin or plugin definition array, this case the 'id' key
   *   is required.
   *
   * @return bool
   *   Returns TRUE if the given plugin is enabled.
   */
  public function isEnabled($plugin_id);

  /**
   * Get the plugin definitions of all enabled plugins.
   *
   * @return array
   *   The key is the plugin ID.
   */
  public function getEnabledDefinitions();

  /**
   * Highlight a code.
   *
   * @param string $code_body
   *   The code to highlight.
   * @param array $options
   *   Highlighting options such as code language or row numbering.
   *
   * @return array|false
   *   Returns FALSE on failure.
   *   Array keys are:
   *   - library: string[]
   *   - body: string
   */
  public function highlight($code_body, array $options = []);

}
