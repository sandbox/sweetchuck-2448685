<?php
/**
 * @file
 * Home of Drupal\codesnippet\Plugin\CodeSnippet\HighlighterBase.
 */

namespace Drupal\codesnippet\Plugin\CodeSnippet;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class HighlighterBase.
 *
 * @package Drupal\codesnippet\Plugin\CodeSnippet
 */
abstract class HighlighterBase extends PluginBase implements HighlighterInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * Pairs of internal language names and labels.
   *
   * Key is the internal language identifier, the value is the human readable
   * label.
   *
   * @var string[]
   */
  protected $internalLanguageLabels = [];

  /**
   * The config factory.
   *
   * @var ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Default highlighter options.
   *
   * @var array
   */
  protected $defaultHighlightOptions = [
    'codelanguage_id' => 'php',
    'output_format' => 'html-css',
    'show_line_numbers' => TRUE,
  ];

  /**
   * Key is the global name, the value is the internal name of an output format.
   *
   * @var string[]
   */
  protected $outputFormatMap = [
    'html' => 'html',
  ];

  /**
   * Constructs an HighlighterBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;

    $this->initOutputFormatMap();
  }

  /**
   * Get default highlighter options.
   *
   * @return array
   *   Key-value pairs.
   */
  public function getDefaultHighlightOptions() {
    return $this->defaultHighlightOptions;
  }

  /**
   * {@inheritdoc}
   */
  public function getColorSchemeMap() {
    $definition = $this->getPluginDefinition();
    $id = $this->getPluginId();
    $config_name = "{$definition['provider']}.highlighter.{$id}.colorscheme_map";

    return $this->configFactory->get($config_name);
  }

  /**
   * Global configuration name.
   *
   * @return string
   *   Configuration name.
   */
  protected function getGlobalConfigName() {
    $definition = $this->getPluginDefinition();

    return "{$definition['provider']}.highlighter.{$definition['id']}.settings";
  }

  /**
   * Get editable global config.
   *
   * @return Config
   *   Config object.
   */
  protected function getEditableGlobalConfig() {
    return $this->configFactory->getEditable($this->getGlobalConfigName());
  }

  /**
   * Get global config.
   *
   * @return Config
   *   Config objecct.
   */
  protected function getGlobalConfig() {
    return $this->configFactory->get($this->getGlobalConfigName());
  }

  /**
   * Initialize the output format mappings.
   *
   * @todo Document.
   */
  protected function initOutputFormatMap() {
    // Nothing to do.
  }

  /**
   * {@inheritdoc}
   */
  abstract public function availableInternalCodeLanguages($environment);

  /**
   * Convert global code language name to internal name.
   *
   * @param string $name
   *   Global code language name.
   *
   * @return string|null
   *   Internal code language name or NULL.
   */
  protected function codeLanguageGlobal2Internal($name) {
    $id = $this->getPluginId();
    $definition = $this->getPluginDefinition();

    return $this->configFactory->get("{$definition['provider']}.highlighter.$id.codelanguage_map")->get($name);
  }

  /**
   * Get the internal name of the output format.
   *
   * @param string $name
   *   Global name of the output format.
   *
   * @return string|null
   *   Internal name.
   */
  protected function getOutputFormat($name) {
    return isset($this->outputFormatMap[$name]) ? $this->outputFormatMap[$name] : NULL;
  }

}
