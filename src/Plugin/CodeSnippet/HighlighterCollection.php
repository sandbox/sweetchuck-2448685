<?php
/**
 * @file
 * Home of \Drupal\codesnippet\Plugin\CodeSnippet\HighlighterCollection.
 */

namespace Drupal\codesnippet\Plugin\CodeSnippet;

use Drupal\Component\Plugin\Mapper\MapperInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Plugin\DefaultLazyPluginCollection;

/**
 * Class HighlighterCollection.
 *
 * @package Drupal\codesnippet\Plugin\CodeSnippet
 */
class HighlighterCollection extends DefaultLazyPluginCollection implements MapperInterface {

  /**
   * {@inheritdoc}
   */
  public function getInstance(array $options) {
    return $this->get($options[$this->pluginKey]);
  }

  /**
   * {@inheritdoc}
   */
  protected function initializePlugin($instance_id) {
    $highlighter_definition = $this->manager->getDefinition($instance_id);
    // Merge the actual configuration into the default configuration.
    if (isset($this->configurations[$instance_id])) {
      $highlighter_definition = NestedArray::mergeDeep($highlighter_definition, $this->configurations[$instance_id]);
    }

    $this->configurations[$instance_id] = $highlighter_definition;

    parent::initializePlugin($instance_id);
  }

}
