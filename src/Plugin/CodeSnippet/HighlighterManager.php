<?php

/**
 * @file
 * Home of \Drupal\codesnippet\Plugin\CodeSnippet\HighlighterManager.
 */

namespace Drupal\codesnippet\Plugin\CodeSnippet;

use Drupal\codesnippet\Entity\ColorSchemeStorageInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class HighlighterManager.
 *
 * @package Drupal\codesnippet\Plugin\CodeSnippet
 */
class HighlighterManager extends DefaultPluginManager implements HighlighterManagerInterface {

  /**
   * The config factory.
   *
   * @var ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Default highlighting options.
   *
   * @var array
   */
  protected $defaultHighlightOptions = [
    'codelanguage_id' => 'php',
    'output_format' => 'html',
    'show_line_numbers' => TRUE,
  ];

  /**
   * Constructs a CodeSnippet Highlighter manager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory) {
    parent::__construct(
      'Plugin/CodeSnippet/Highlighter',
      $namespaces,
      $module_handler,
      'Drupal\codesnippet\Plugin\CodeSnippet\HighlighterInterface',
      'Drupal\codesnippet\Annotation\CodeSnippetHighlighter'
    );

    $this->configFactory = $config_factory;
    $this->setCacheBackend($cache_backend, 'codesnippet_highlighter_plugins');

    $this->alterInfo('codesnippet_highlighter_info');

    $this->mapper = new HighlighterCollection($this);
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled($plugin_id) {
    if (is_array($plugin_id)) {
      $plugin_id = $plugin_id['id'];
    }

    return $this->configFactory->get("codesnippet.highlighter.$plugin_id")->get('enabled');
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

    $definition['weight'] = $this->configFactory
      ->get("codesnippet.highlighter.$plugin_id")
      ->get('weight');
  }

  /**
   * {@inheritdoc}
   */
  protected function findDefinitions() {
    $definitions = parent::findDefinitions();
    uasort($definitions, ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);

    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnabledDefinitions() {
    return array_filter($this->getDefinitions(), [$this, 'isEnabled']);
  }

  /**
   * Default highlighting options.
   *
   * @return array
   *   The common keys are:
   *   - codelanguage_id: php
   *   - output_format: html
   *   - show_line_numbers: TRUE
   */
  public function getDefaultHighlightOptions() {
    return $this->defaultHighlightOptions;
  }

  /**
   * {@inheritdoc}
   */
  public function highlight($code_body, array $options = []) {
    $options += $this->getDefaultHighlightOptions();
    $highlighter_definitions = $this->getEnabledDefinitions();
    if (!empty($options['highlighter_id'])) {
      if (!isset($highlighter_definitions[$options['highlighter_id']])) {
        throw new \Exception('@todo');
      }

      $highlighter_definitions = [
        $options['highlighter_id'] => $highlighter_definitions[$options['highlighter_id']],
      ];
    }

    $wrapper_classes = [
      'base' => 'codesnippet',
      'language' => 'language-' . Html::getClass($options['codelanguage_id']),
      'show_line_numbers' => 'line-numbers-' . ($options['show_line_numbers'] ? 'yes' : 'no'),
    ];

    // @todo Themeable.
    $snippet_prefix_pattern = '<div class="%s">';
    $snippet_suffix = '</div>';
    foreach ($highlighter_definitions as $highlighter_id => $highlighter_definition) {
      /** @var HighlighterInterface $highlighter */
      $highlighter = $this->getInstance($highlighter_definition);
      $highlight_result = $highlighter->highlight($code_body, $options);
      if ($highlight_result) {
        /** @var ColorSchemeStorageInterface $colorscheme_storage */
        $colorscheme_storage = \Drupal::entityManager()->getStorage('codesnippet_colorscheme');

        $wrapper_classes['highlighter'] = 'highlighter-' . Html::getClass($highlighter_id);

        if (empty($options['colorscheme'])) {
          $options['colorscheme'] = $colorscheme_storage->getFirst($highlighter_id);
        }

        $colorscheme_library = NULL;
        if (!empty($options['colorscheme'])) {
          $colorscheme_library = $highlighter
            ->getColorSchemeMap()
            ->get($options['colorscheme']->id());
        }

        if ($colorscheme_library) {
          $highlight_result['library'][] = $colorscheme_library;
          list($colorscheme_library_provider) = explode('/', $colorscheme_library);
          $wrapper_classes['scheme-provider'] = 'scheme-' . Html::getClass($colorscheme_library_provider);
          $wrapper_classes['scheme-full'] = 'scheme-' . Html::getClass($colorscheme_library);
        }

        $snippet_prefix = sprintf($snippet_prefix_pattern, implode(' ', $wrapper_classes));
        $highlight_result['body'] = $snippet_prefix . $highlight_result['body'] . $snippet_suffix;

        return $highlight_result;
      }
    }

    return FALSE;
  }

}
