<?php

/**
 * @file
 * Home of Drupal\codesnippet\Plugin\CodeSnippet\HighlighterInterface.
 */

namespace Drupal\codesnippet\Plugin\CodeSnippet;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;

/**
 * Interface HighlighterInterface.
 *
 * @package Drupal\codesnippet\Plugin\CodeSnippet
 */
interface HighlighterInterface extends ContainerFactoryPluginInterface, PluginInspectionInterface {

  /**
   * Get the available internal code language identifiers.
   *
   * @param string $environment
   *   One of
   *   - NULL
   *   - "client_js"
   *   - "backend".
   *
   * @return string[]
   *   Machine name of the available code languages.
   *   Key and the value is same.
   */
  public function availableInternalCodeLanguages($environment);

  /**
   * Helper function for the filter plugins.
   *
   * @param FilterProcessResult $result
   *   The filtered text, wrapped in a FilterProcessResult object, and possibly
   *   with associated assets, cacheability metadata and placeholders.
   * @param string $code_body_flat
   *   Code body to highlight.
   * @param array $options
   *   Available keys are:
   *     - codelanguage_id: string
   *     - output_format: string.
   *
   * @return array|false
   *   Keys:
   *     - body: string
   */
  public function filterProcess(FilterProcessResult $result, $code_body_flat, array $options);

  /**
   * Highlight a piece of code.
   *
   * @param string $code_body
   *   Code body to highlight.
   * @param array $options
   *   Available keys are:
   *     - codelanguage_id: string
   *     - output_format: string.
   *
   * @return array|false
   *   Return string on success, FALSE on failure.
   *   - library: string[]
   *   - body: string
   */
  public function highlight($code_body, array $options = []);

  /**
   * Documentation missing.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   Color scheme map configuration.
   */
  public function getColorSchemeMap();

}
