<?php
/**
 * @file
 * Home of \Drupal\codesnippet_gnushl\Plugin\CodeSnippet\Highlighter\GnuShl.
 */

namespace Drupal\codesnippet_gnushl\Plugin\CodeSnippet\Highlighter;

use Drupal\codesnippet\System;
use Drupal\Core\Annotation\Translation;
use Drupal\codesnippet\Annotation\CodeSnippetHighlighter;
use Drupal\codesnippet\Plugin\CodeSnippet\HighlighterBase;
use Drupal\filter\FilterProcessResult;

/* @codingStandardsIgnoreStart */
/**
 * GNU Source-highlight executor.
 *
 * @see http://www.gnu.org/software/src-highlite/source-highlight.html
 *
 * @CodeSnippetHighlighter(
 *   id = "gnushl",
 *   title = @Translation("GNU Source-highlight"),
 *   description = @Translation("CLI tool of GNU Source-highlight."),
 *   links = {
 *     "settings"         = "codesnippet_gnushl.admin.highlighter.gnushl.settings",
 *     "codelanguage_map" = "codesnippet_gnushl.admin.highlighter.gnushl.codelanguage_map"
 *   }
 * )
 *
 * @codingStandardsIgnoreEnd
 *
 * @package Drupal\codesnippet_gnushl\Plugin\CodeSnippet\highlighter
 */
class GnuShl extends HighlighterBase {
  /* @codingStandardsIgnoreEnd */

  use System;

  /**
   * {@inheritdoc}
   */
  protected $internalLanguageLabels = [
    'ada' => 'ADA',
    'applescript' => 'AppleScript',
    'asm' => 'ASM',
    'awk' => 'AWK',
    'bat' => 'Batch',
    'c' => 'C',
    'caml' => 'Caml',
    'changelog' => 'Changelog',
    'clipper' => 'Clipper',
    'cobol' => 'Cobol',
    'conf' => 'Conf',
    'cpp' => 'C++',
    'csharp' => 'C#',
    'css' => 'CSS',
    'd' => 'D',
    'desktop' => 'Desktop',
    'diff' => 'Diff or patch',
    'erlang' => 'Erlang',
    'fixed-fortran' => 'Fortran (fixed)',
    'flex' => 'Flex',
    'fortran' => 'Fortran',
    'haskell' => 'Haskell',
    'haskell_literate' => 'Haskell (literate)',
    'html' => 'HTML',
    'java' => 'Java',
    'javalog' => 'Java Log',
    'javascript' => 'JavaScript',
    'latex' => 'LaTex',
    'lisp' => 'Lisp',
    'log' => 'Log',
    'logtalk' => 'logtalk',
    'lua' => 'Lua',
    'makefile' => 'Makefile',
    'manifest' => 'Manifest',
    'pascal' => 'Pascal',
    'perl' => 'Perl',
    'php' => 'PHP',
    'po' => 'Gettext',
    'postscript' => 'Postscript',
    'prolog' => 'ProLog',
    'properties' => 'Properties',
    'python' => 'Python',
    'ruby' => 'Ruby',
    'scala' => 'Scala',
    'scheme' => 'Scheme',
    'sql' => 'SQL',
    'style' => 'Style',
    'vbscript' => 'VB Script',
    'xml' => 'XML',
    'xorg' => 'Xorg Configuration',
  ];

  /**
   * {@inheritdoc}
   */
  public function filterProcess(FilterProcessResult $result, $code_body_flat, array $options) {
    return $this->highlight($code_body_flat, $options);
  }

  /**
   * Transform the raw code into a highlighted one.
   *
   * @param string $code_body
   *   Raw source code.
   * @param array $options
   *   Configuration options.
   *
   * @return string|false
   *   The highlighted code or FALSE on failure.
   */
  public function highlight($code_body, array $options = []) {
    $options += $this->getDefaultHighlightOptions();
    $options['codelanguage_id'] = $this->codeLanguageGlobal2Internal($options['codelanguage_id']);
    $options['output_format'] = $this->getOutputFormat($options['output_format']);

    $config = $this->getGlobalConfig();
    $source_highlight_path = $config->get('source_highlight_path');

    if (!$options['codelanguage_id']
      || !$options['output_format']
      || !$source_highlight_path
    ) {
      return FALSE;
    }

    $cmd_pattern = 'echo %s | %s --src-lang=%s --out-format=%s';
    $cmd_args = [
      escapeshellarg($code_body),
      escapeshellcmd($source_highlight_path),
      escapeshellarg($options['codelanguage_id']),
      escapeshellarg($options['output_format']),
    ];

    if ($options['show_line_numbers']) {
      $cmd_pattern .= ' --line-number=" "';
    }

    $result = $this->exec(vsprintf($cmd_pattern, $cmd_args));

    return $result['error_code'] ? FALSE : ['body' => implode("\n", $result['output'])];
  }

  /**
   * {@inheritdoc}
   */
  protected function initOutputFormatMap() {
    $this->outputFormatMap['html'] = 'html-css';
  }

  /**
   * {@inheritdoc}
   */
  public function availableInternalCodeLanguages($environment) {
    switch ($environment) {
      case NULL:
      case 'backend':
        // @todo Cache.
        $config = $this->getGlobalConfig();
        $cmd_pattern = '%s --lang-list';
        $cmd_args = [
          escapeshellcmd($config->get('source_highlight_path')),
        ];

        $result = $this->exec(vsprintf($cmd_pattern, $cmd_args));

        if ($result['error_code']) {
          return [];
        }

        $code_languages = [];
        foreach ($result['output'] as $line) {
          $matches = NULL;
          if (preg_match('/ = (?P<id>.+?)\.lang$/', $line, $matches)) {
            $code_languages[$matches['id']] = $matches['id'];
          }
        }
        $code_languages = array_intersect_key($this->internalLanguageLabels, $code_languages) + $code_languages;
        asort($code_languages, SORT_NATURAL | SORT_FLAG_CASE);

        return $code_languages;

    }

    return [];
  }

}
