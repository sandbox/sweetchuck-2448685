<?php
/**
 * @file
 * Home of \Drupal\codesnippet_gnushl\Form\SettingsForm.
 */

namespace Drupal\codesnippet_gnushl\Form;

use Drupal\codesnippet\System;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\codesnippet_gnushl\Form
 */
class HighlighterGnushlSettingsForm extends ConfigFormBase {

  use System;

  /**
   * Global configuration name.
   *
   * @var string
   */
  protected $configName = 'codesnippet_gnushl.highlighter.gnushl.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'codesnippet_gnushl_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config($this->configName);

    $form['source_highlight_path'] = [
      '#type' => 'textfield',
      '#title' => t('GNU Source-highlight path'),
      '#required' => TRUE,
      '#default_value' => $config->get('source_highlight_path'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $source_highlight_path = $form_state->getValue('source_highlight_path');
    $source_highlight_version = $this->sourceHighlightVersion($source_highlight_path);
    if ($source_highlight_version) {
      $form_state->set(['codesnippet_gnushl', 'source_highlight_version'], $source_highlight_version);
    }
    else {
      $form_state->setErrorByName('source_highlight_path', $this->t('GNU Source-highlight version cannot be determined'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config($this->configName);
    $config
      ->set('source_highlight_path', $form_state->getValue('source_highlight_path'))
      ->save();

    $source_highlight_version = $form_state->get(['codesnippet_gnushl', 'source_highlight_version']);
    drupal_set_message($this->t('GNU Source-highlight version is @version', ['@version' => $source_highlight_version]));

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [$this->configName];
  }

  /**
   * Detect the version of the source-highlight executable.
   *
   * @param string $source_highlight_path
   *   File path.
   *
   * @return string|bool
   *   Version string or FALSE.
   */
  protected function sourceHighlightVersion($source_highlight_path) {
    $cmd = sprintf('%s --version', escapeshellcmd($source_highlight_path));

    return $this->detectVersion($cmd, '/^GNU Source-highlight (?P<version>\d.+?)( |$)/');
  }

  /**
   * Detect the version number in the output.
   *
   * @param string $command
   *   The command that will be executed.
   * @param string $pattern
   *   Regular expression with a named group "version".
   * @param int $line_index
   *   Number of the line in the output which contains the version number.
   *
   * @return string|bool
   *   Version string or FALSE.
   */
  protected function detectVersion($command, $pattern, $line_index = 0) {
    $matches = [];
    $result = $this->exec($command);

    return ($result['error_code'] === 0
      && isset($result['output'][$line_index])
      && preg_match($pattern, $result['output'][$line_index], $matches)
    ) ? $matches['version'] : FALSE;
  }

}
