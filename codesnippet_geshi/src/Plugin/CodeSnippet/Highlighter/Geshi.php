<?php
/**
 * @file
 * Home of \Drupal\codesnippet_geshi\Plugin\CodeSnippet\Highlighter\Geshi.
 */

namespace Drupal\codesnippet_geshi\Plugin\CodeSnippet\Highlighter;

use Drupal\Core\Annotation\Translation;
use Drupal\codesnippet\Annotation\CodeSnippetHighlighter;
use Drupal\codesnippet\Plugin\CodeSnippet\HighlighterBase;
use Drupal\filter\FilterProcessResult;

/* @codingStandardsIgnoreStart */
/**
 * GeSHi Source-highlight executor.
 *
 * @see https://github.com/GeSHi/geshi-1.0
 *
 * @CodeSnippetHighlighter(
 *   id = "geshi",
 *   title = @Translation("GeSHi"),
 *   description = @Translation("Generic Syntax Highlighter for PHP."),
 *   links = {
 *     "codelanguage_map" = "codesnippet_geshi.admin.highlighter.geshi.codelanguage_map"
 *   }
 * )
 *
 * @package Drupal\codesnippet_geshi\Plugin\CodeSnippet\highlighter
 */
class Geshi extends HighlighterBase {
  /* @codingStandardsIgnoreEnd */

  /**
   * {@inheritdoc}
   */
  protected $internalLanguageLabels = [
    'abap' => 'Adap',
    'actionscript' => 'ActionScript',
    'actionscript3' => 'ActionScript 3',
    'ada' => 'ADA',
    'algol68' => 'ALGOL 68',
    'apache' => 'Apache configuration',
    'applescript' => 'Apple script',
    'apt_sources' => 'APT Sources',
    'arm' => 'ARM',
    'asm' => 'ASM',
    'asp' => 'ASP',
    'asymptote' => 'Asymptote',
    'autoconf' => 'Autoconf',
    'autohotkey' => 'AutoHotkey',
    'autoit' => 'AutoIt',
    'avisynth' => 'AviSynth',
    'awk' => 'AWK',
    'bascomavr' => 'BASCOM-AVR',
    'bash' => 'Bash',
    'basic4gl' => 'Basic4GL',
    'bf' => 'Brainfuck',
    'bibtex' => 'BibTex',
    'blitzbasic' => 'BlitzBasic',
    'bnf' => 'BNF (Backus-Naur form)',
    'boo' => 'Boo',
    'c' => 'C',
    'c_loadrunner' => 'C Loadrunner',
    'c_mac' => 'C Mac',
    'c_winapi' => 'C Win API',
    'caddcl' => 'CAD Dialog Control Language',
    'cadlisp' => 'CAD Lisp',
    'cfdg' => 'CFDG (Context Free Design Grammar)',
    'cfm' => 'ColdFusion',
    'chaiscript' => 'ChaiScript',
    'chapel' => 'Chapel',
    'cil' => 'CIL - Common Intermediate Language',
    'clojure' => 'Clojure',
    'cmake' => 'CMake',
    'cobol' => 'Cobol',
    'coffeescript' => 'CoffeScript',
    'cpp-qt' => 'C++ Qt',
    'cpp-winapi' => 'C++ WinAPI',
    'cpp' => 'C++',
    'csharp' => 'C#',
    'css' => 'CSS',
    'cuesheet' => 'CUE Sheet',
    'd' => 'D',
    'dart' => 'Dart',
    'dcl' => 'DCL',
    'dcpu16' => 'DCPU/16 Assembly',
    'dcs' => 'DCS (Data Conversion System)',
    'delphi' => 'Delphi',
    'diff' => 'Diff or patch',
    'div' => 'DIV',
    'dos' => 'DOS',
    'dot' => 'DOT',
    'e' => 'E',
    'ecmascript' => 'EcmaScript',
    'eiffel' => 'Eiffel',
    'email' => 'Email (mbox \ eml \ RFC format)',
    'epc' => 'Enerscript',
    'erlang' => 'Erlang',
    'euphoria' => 'Euphoria',
    'ezt' => 'Easytrieve',
    'f1' => 'Formula One',
    'falcon' => 'http://www.falconpl.org',
    'fo' => 'FO',
    'fortran' => 'Fortran',
    'freebasic' => 'FreeBasic',
    'freeswitch' => 'FreeSwitch',
    'fsharp' => 'F#',
    'gambas' => 'Gambas',
    'gettext' => 'Gettext',
    'gnuplot' => 'Gnuplot',
    'go' => 'Go',
    'groovy' => 'Groovy',
    'haskell' => 'Haskell',
    'html4strict' => 'HTML 4 Strict',
    'html5' => 'HTML 5',
    'j' => 'J',
    'java' => 'Java',
    'java5' => 'Java 5',
    'javascript' => 'JavaScript',
    'jquery' => 'jQuery',
    'latex' => 'LaTex',
    'lisp' => 'Lisp',
    'lotusformulas' => 'Lotus Formulas',
    'lotusscript' => 'Lotus Script',
    'lua' => 'Lua',
    'make' => 'Make',
    'mysql' => 'MySQL',
    'pascal' => 'Pascal',
    'pcre' => 'PCRE',
    'perl' => 'Perl',
    'perl6' => 'Perl 6',
    'php-brief' => 'PHP-Brief',
    'php' => 'PHP',
    'plsql' => 'PLSQL',
    'postgresql' => 'PostgreSQL',
    'postscript' => 'PostScript',
    'povray' => 'PovRay',
    'qbasic' => 'QBasic',
    'qml' => 'QML',
    'robots' => 'robots.txt',
    'rpmspec' => 'RPM Spec',
    'rsplus' => 'R',
    'ruby' => 'Ruby',
    'scala' => 'Scala',
    'smalltalk' => 'SmallTalk',
    'smarty' => 'Smarty',
    'sparql' => 'SparQL',
    'sql' => 'SQL',
    'stonescript' => 'StoneScript',
    'systemverilog' => 'SystemVeriLog',
    'typoscript' => 'TypoScript',
    'vb' => 'Visual Basic',
    'vbnet' => 'VB.Net',
    'vbscript' => 'VB Script',
    'verilog' => 'VeriLog',
    'visualfoxpro' => 'Visual FoxPro',
    'visualprolog' => 'Visual ProLog',
    'winbatch' => 'Batch',
    'xbasic' => 'XBasic',
    'xml' => 'XML',
    'xorg_conf' => 'Xorg configuration',
    'xpp' => 'X++',
    'yaml' => 'Yaml',
    'z80' => 'Z80',
    'zxbasic' => 'ZX Basic',
  ];

  /**
   * {@inheritdoc}
   */
  public function filterProcess(FilterProcessResult $result, $code_body_flat, array $options) {
    return $this->highlight($code_body_flat, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function highlight($code_body, array $options = []) {
    $options += $this->getDefaultHighlightOptions();

    $options['codelanguage_id'] = $this->codeLanguageGlobal2Internal($options['codelanguage_id']);
    $options['output_format'] = $this->getOutputFormat($options['output_format']);

    if (!$this->loadGeshi()
      || !$options['codelanguage_id']
      || !$options['output_format']
    ) {
      return FALSE;
    }

    $geshi = new \GeSHi($code_body, $options['codelanguage_id']);
    $geshi->enable_classes();
    $geshi->enable_line_numbers($options['show_line_numbers'] ? GESHI_NORMAL_LINE_NUMBERS : GESHI_NO_LINE_NUMBERS);
    $geshi->set_header_type(GESHI_HEADER_PRE_VALID);

    return [
      'body' => str_replace('&nbsp;', ' ', $geshi->parse_code()),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function availableInternalCodeLanguages($environment) {
    switch ($environment) {
      case NULL:
      case 'backend':
        if (!$this->loadGeshi()) {
          return [];
        }

        // @todo Cache.
        $geshi = new \GeSHi('<?php echo "foo";', 'php');
        $code_languages = $geshi->get_supported_languages();
        $code_languages = array_combine($code_languages, $code_languages);
        $code_languages = array_intersect_key($this->internalLanguageLabels, $code_languages) + $code_languages;
        asort($code_languages, SORT_NATURAL | SORT_FLAG_CASE);

        return $code_languages;
    }

    return [];
  }

  /**
   * Load the GeSHi library.
   *
   * @todo Watch what will happen with the libraries.
   *
   * @see https://www.drupal.org/node/773508
   *
   * @return bool
   *   GeSHi is available or not.
   */
  protected function loadGeshi() {
    if (class_exists('\GeSHi')) {
      return TRUE;
    }

    // @todo Catch the error and log.
    require_once DRUPAL_ROOT . '/libraries/geshi/src/geshi.php';

    return class_exists('\GeSHi');
  }

}
