<?php

/**
 * @file
 * Contains \Drupal\codesnippet_filter\Plugin\Filter\CodeSnippet.
 */

namespace Drupal\codesnippet_filter\Plugin\Filter;

use Drupal\codesnippet\Plugin\CodeSnippet\HighlighterManagerInterface;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\Annotation\Filter;
use Drupal\Component\Utility\Html;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Code Snippet highlighter.
 *
 * Text filter to syntax highlight the code snippets in long formatted texts.
 *
 * @Filter(
 *   id = "codesnippet",
 *   title = @Translation("Code Snippet syntax highlighter"),
 *   description = @Translation("Source code syntax highlighter."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 *   settings = {
 *     "options" = {
 *       "show_line_numbers" = TRUE
 *     },
 *     "highlighters" = {}
 *   }
 * )
 */
class CodeSnippet extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['options'] = [
      '#type' => 'details',
      '#tree' => TRUE,
      '#title' => t('Options'),
      '#open' => TRUE,
    ];

    $form['options']['show_line_numbers'] = [
      '#type' => 'checkbox',
      '#title' => t('Show line numbers'),
      '#default_value' => $this->settings['options']['show_line_numbers'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    // @todo Description.
    if ($long) {
      return $this->t('This is the long description of the Syntax Highlighter filter');
    }
    else {
      return $this->t('This is the short description of the Syntax Highlighter filter');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    /** @var HighlighterManagerInterface $highlighter_manager */
    $highlighter_manager = \Drupal::service('plugin.manager.codesnippet.highlighter');

    $dom = Html::load($text);
    $xpath = new \DOMXPath($dom);

    $result = new FilterProcessResult($text);

    $code_nodes = $xpath->query('//pre/code');
    for ($i = 0; $i < $code_nodes->length; $i++) {
      /** @var \DOMElement $code_node */
      $code_node = $code_nodes->item($i);
      $options = [
        'output_format' => 'html',
        'codelanguage_id' => NULL,
        'show_line_numbers' => $this->settings['options']['show_line_numbers'],
      ];
      $matches = NULL;
      if ($code_node->hasAttribute('class')
        && preg_match('/(^|\s)language-(?P<language>\S+)/', $code_node->getAttribute('class'), $matches)
      ) {
        $options['codelanguage_id'] = $matches['language'];
      }

      $highlight_result = $highlighter_manager->highlight($code_node->textContent, $options);
      if ($highlight_result) {
        if (!empty($highlight_result['library'])) {
          $result->addAssets(['library' => $highlight_result['library']]);
        }

        $code_node_highlighted = $dom->createDocumentFragment();
        $code_node_highlighted->appendXML($highlight_result['body']);
        $code_node->parentNode->parentNode->replaceChild($code_node_highlighted, $code_node->parentNode);

        break;
      }
    }

    $result->setProcessedText(Html::serialize($dom));

    return $result;
  }

}
