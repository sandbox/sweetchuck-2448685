<?php
/**
 * @file
 * Home of \Drupal\codesnippet\Plugin\CKEditorPlugin\CodeSnippet.
 */

namespace Drupal\codesnippet_ckeditor\Plugin\CKEditorPlugin;

use Drupal\ckeditor\Annotation\CKEditorPlugin;
use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\Core\Annotation\Translation;
use Drupal\editor\Entity\Editor;

/**
 * Class CodeSnippet.
 *
 * @CKEditorPlugin(
 *   id = "codesnippet",
 *   label = @Translation("Code snippet highlighter"),
 * )
 *
 * @package Drupal\codesnippet\Plugin\CKEditorPlugin
 */
class CodeSnippet extends CKEditorPluginBase {

  /**
   * Filesystem path to the CKEditor plugin.
   *
   * @todo Get this path dynamically with Libraries API module.
   *
   * @see http://ckeditor.com/addon/codesnippet
   *
   * @var string
   */
  protected $pathToPlugin = '/libraries/ckeditor.codesnippet';

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return "{$this->pathToPlugin}/plugin.js";
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'CodeSnippet' => [
        'label' => t('Insert Code Snippet'),
        'image' => "{$this->pathToPlugin}/icons/codesnippet.png",
      ],
    ];
  }

}
